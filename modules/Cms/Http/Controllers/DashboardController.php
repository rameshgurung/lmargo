<?php namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use Modules\Cms\Entities\Setting;


use Breadcrumbs;

class DashboardController extends Controller {
	

	public function __construct(){
	if (! Breadcrumbs::exists('dashboard')) {
		Breadcrumbs::register('dashboard', function($breadcrumbs)
			{
				$breadcrumbs->push('Dashboard', route('dashboard'));
			});
		}
	}

	public function index()
	{		
		$settings = get_settings();
		$categories = get_categories();
		$posts = get_posts();

		$active_setting_tab = Session::get('active_setting_tab')?Session::get('active_setting_tab'):1;

		return View::make('cms::dashboard' , compact('categories', 'posts', 'settings', 'active_setting_tab'));
	}


	public function settingUpdate(Request $request)
	{		
		$settings = $request->all();
		// dd($settings);		
		$settings['maintenance_mode_enabled'] = array_key_exists('maintenance_mode_enabled', $settings) ? 1 : 0;
		$settings['mailchimp_newsletter_enabled'] = array_key_exists('mailchimp_newsletter_enabled', $settings) ? 1 : 0;

		$skip = array('_token', 'header');

		// current tab
		if($request->exists('header')){
			$tab = 1;
		}
		elseif($request->exists('footer')){
			$tab = 2;
		}
		elseif($request->exists('sidebar')){
			$tab = 3;
		}
		elseif($request->exists('contact')){
			$tab = 4;
		}
		else{
			$tab = 5;
		}
		Session::set('active_setting_tab', $tab);

		foreach ($settings as $key => $value) {

			$setting_slug = $key;
			$setting_value = $value;

			if(!in_array($setting_slug, $skip) || $value != "Save"){
				Setting::where('slug','=',$setting_slug)->update(['value' => $setting_value]);
			}
		}

		// redirect
		Session::flash('success', 'Dashboard Setting updated!');
		return Redirect::route('dashboard');

	}


	
}
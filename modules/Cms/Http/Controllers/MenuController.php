<?php

namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Validator;

use Breadcrumbs;
use Modules\Cms\Entities\Menu;
use Modules\Cms\Entities\Category;
use Modules\Cms\Entities\Post;
use App\User;
use DB;

use Modules\Cms\Http\Controllers\DashboardController;

class MenuController extends DashboardController
{


    public function __construct(Request $request)
    {
        parent::__construct();

        if (!Breadcrumbs::exists('dashboard.menu.index')) {
            Breadcrumbs::register('dashboard.menu.index', function ($breadcrumbs) {
                $breadcrumbs->parent('dashboard');
                $breadcrumbs->push('Menu', route('dashboard.menu.index'));
            });
        }

        // footer menu
        // echo $request->url();
        if (strpos($request->url(), '/menu/footer')) {
            if (!Breadcrumbs::exists('dashboard.menu.footer.index')) {
                Breadcrumbs::register('dashboard.menu.footer.index', function ($breadcrumbs) {
                    $breadcrumbs->parent('dashboard');
                    $breadcrumbs->push('Menu', route('dashboard.menu.index'));
                    $breadcrumbs->push('Footer menu', route('dashboard.menu.footer.index'));
                });
            }
        }

        DB::enableQueryLog();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Menu::has('category', 'post', 'author')->where('title', '=', 'Services')->get());  

        $query = Menu::where(Menu::$table_name . '.id', 2);
        // $menus = Menu::where('published', 1)->get();          
        $query = Menu::where(Menu::$table_name . '.published', 1);
        $query = $query->where(Menu::$table_name . '.menu_type', Menu::UPPER_MENU);
        $query = $query->where(Menu::$table_name . '.level', 0);

        $query = $query->leftJoin(Category::$table_name, Menu::$table_name . '.category_id', '=', Category::$table_name . '.id');
        $query = $query->leftJoin(Post::$table_name, Menu::$table_name . '.post_id', '=', Post::$table_name . '.id');

        $menus = $query->get([Menu::$table_name . '.*']);

        // $quries = DB::getQueryLog();
        // print_r($quries);
        // dd($menus);

        return View::make('cms::menu.backend.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = new Menu();

        list($categories, $posts, $upper_parent_menus, $menu_page_types, $menu_types) = $this->get_select_data();

        Breadcrumbs::register('dashboard.menu.create', function ($breadcrumbs) {
            $breadcrumbs->parent('dashboard.menu.index');
            $breadcrumbs->push('Create');
        });
        return View::make('cms::menu.backend.create', compact('menu', 'categories', 'posts', 'upper_parent_menus', 'menu_page_types', 'menu_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $rules =  array(
            'title' => 'required|max:255',
            'page_type' => 'required',
            'menu_type' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::route('dashboard.menu.create')->withErrors($validator);
        } else {

            $data = request()->all();
            $data = $this->set_menu_data($data);
            $data['author_id'] = auth()->id();

            Menu::create($data);

            // redirect
            Session::flash('success', 'Successfully created!');
            return Redirect::route('dashboard.menu.index');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        list($categories, $posts, $upper_parent_menus, $menu_page_types, $menu_types) = $this->get_select_data();
        // dd($upper_parent_menus);

        // get the category
        $menu = Menu::find($id);
        // dd($menu);
        if (!$menu) {
            Session::flash('error', 'Category not found');
            return Redirect::route('dashboard.menu.index');
        }

        Breadcrumbs::register('dashboard.menu.edit', function ($breadcrumbs) {
            $breadcrumbs->parent('dashboard.menu.index');
            $breadcrumbs->push('Edit');
        });
        // show the edit form and pass the category
        return View::make('cms::menu.backend.update', compact('menu', 'categories', 'posts', 'upper_parent_menus', 'menu_page_types', 'menu_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        $rules =  array(
            'title' => 'required|max:255',
            'page_type' => 'required',
            'menu_type' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::route('dashboard.menu.edit', $id)->withErrors($validator);
        } else {

            // update
            $menu = Menu::find($id);
            $data = request()->all();
            $data = $this->set_menu_data($data);
            // dd($data);
            $menu->update($data);

            // redirect
            Session::flash('success', 'Successfully updated!');
            return Redirect::route('dashboard.menu.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $menu = Menu::find($id);
        $menu->delete();

        // redirect
        Session::flash('success', 'Successfully deleted!');
        return Redirect::route('dashboard.menu.index');
    }

    public function order()
    {
        $menus = Menu::where('published', 1)->get();
        // // delete
        // $menu = Menu::find($id);
        // $menu->delete();

        // // redirect
        // Session::flash('success', 'Successfully deleted!');
        // return Redirect::route('dashboard.menu.order');
        Breadcrumbs::register('dashboard.menu.order', function ($breadcrumbs) {
            $breadcrumbs->parent('dashboard.menu.index');
            $breadcrumbs->push('Order');
        });
        return View::make('cms::menu.backend.order', compact('menus'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function footerindex()
    {
        // $menus = Menu::All();  
        $menus = Menu::where('published', 1)->paginate(10);
        return View::make('cms::menu.backend.footer.index', compact('menus'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function footercreate()
    {

        $menu = new Menu();
        Breadcrumbs::register('dashboard.menu.footer.create', function ($breadcrumbs) {
            $breadcrumbs->parent('dashboard.menu.footer.index');
            $breadcrumbs->push('Create');
        });
        return View::make('cms::menu.backend.footer.create', compact('menu'));
    }

    private function get_select_data()
    {
        $categories = get_categories()->lists('title', 'id')->toArray();
        array_unshift($categories, 'Please Select');

        $posts = get_posts()->lists('title', 'id')->toArray();
        array_unshift($posts, 'Please Select');

        $upper_parent_menus = get_upper_parent_menus();

        $menu_page_types = get_menu_page_types();
        array_unshift($menu_page_types, 'Please Select');

        $menu_types = get_menu_types();
        array_unshift($menu_types, 'Please Select');

        return array($categories, $posts, $upper_parent_menus, $menu_page_types, $menu_types);
    }

    private function set_menu_data($data)
    {

        $data['title'] = ucfirst($data['title']);
        $data['slug'] = str_slug($data['title']);
        $data['url'] = str_slug($data['url']);
        $data['url'] = strpos($data['url'], '/') ?: '/' . $data['url'];
        $data['breadcrumb'] = ucfirst($data['breadcrumb']);
        $data['level'] = !$data['parent_menu_id'] ? 0 : 1;
        $data['published'] = isset($data['published']) ?: 0;
        $data['sidebar'] = isset($data['sidebar']) ?: 0;
        
        return $data;
    }
}

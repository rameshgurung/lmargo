<?php namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Modules\Cms\Entities\Category;
use Illuminate\Support\Facades\View;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Validator;

use Breadcrumbs;
use Modules\Cms\Http\Controllers\DashboardController;

class CategoryController extends DashboardController {
	

    public function __construct(){
        parent::__construct();
    
        if (! Breadcrumbs::exists('dashboard.category.index')) {    
            Breadcrumbs::register('dashboard.category.index', function($breadcrumbs)
            {
                $breadcrumbs->parent('dashboard');
                $breadcrumbs->push('Category', route('dashboard.category.index'));
            });
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	// $categories = Category::All();  
        $categories = Category::where('published', 1)->paginate(10);                

        return View::make('cms::category.backend.index' , compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();       
        Breadcrumbs::register('dashboard.category.create', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.category.index');
            $breadcrumbs->push('Create', route('dashboard.category.create'));
        });
        return View::make('cms::category.backend.create' , compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $rules =  array(
            'title' => 'required|max:255',
            'content' => 'required|max:5000',
            );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::route('dashboard.category.create')->withErrors($validator);
        } else {

            // update
            $category = new Category;
            $category->title = Input::get('title');
            $category->content = Input::get('content');
            // $category->published = 1;
            
            $category->save();

            // redirect
            Session::flash('success', 'Successfully created!');
            return Redirect::route('dashboard.category.index');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // get the category
        $category = Category::find($id);
        if(!$category){
            Session::flash('error', 'Category not found');
            return Redirect::route('dashboard.category.index');
        }

        Breadcrumbs::register('dashboard.category.edit', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.category.index');
            $breadcrumbs->push('Edit');
        });
        // show the edit form and pass the category
        return View::make('cms::category.backend.update' , compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        $rules =  [
        'title' => 'required|max:255',
        'content' => 'required|max:5000',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::route('dashboard.category.edit', $id)->withErrors($validator);
        } else {

            // update
            $category = Category::find($id);
            $category->title = Input::get('title');
            $category->content = Input::get('content');
            
            $category->save();

            // redirect
            Session::flash('success', 'Successfully updated!');
            return Redirect::route('dashboard.category.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $category = Category::find($id);
        $category->delete();

        // redirect
        Session::flash('success', 'Successfully deleted!');
        return Redirect::route('dashboard.category.index');
    }

    /**
     * Handle Method does not exist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::flash('error', 'Page not found!');
        return Redirect::route('dashboard.category.index');
    }
}
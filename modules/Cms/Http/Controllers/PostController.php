<?php namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Modules\Cms\Entities\Category;
use Modules\Cms\Entities\Post;
use Illuminate\Foundation\Auth\User;

use Illuminate\Support\Facades\View;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Validator;
use DB;

use Breadcrumbs;
use Modules\Cms\Http\Controllers\DashboardController;

class PostController extends DashboardController {
	

    public function __construct(){
        parent::__construct();
        // to solve php artisan route:list error
        // error : Breadcrumb name "dashboard.post.index" has already been registered
        if (! Breadcrumbs::exists('dashboard.post.index')) {    
            Breadcrumbs::register('dashboard.post.index', function($breadcrumbs)
            {
                $breadcrumbs->parent('dashboard');
                $breadcrumbs->push('Post', route('dashboard.post.index'));
            });
        }
        DB::enableQueryLog();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // echo public_path('uploads/filemanager/source/');
        // die;

    	$categories = Category::All();  
        $users = User::All();  


        // 0
        // $posts = Post::where('published', 1)->paginate(10);

        // 1
        // $posts = Post::where('published', 1)
        //         ->leftJoin(Post::$stable, 'tbl_post.category_id', '=', 'tbl_category.id')
        //         ->leftJoin('users', 'tbl_post.author_id', '=', 'users.id')
        //         ->where('tbl_post.category_id', \Request::get('category_id'))
        //         ->where('tbl_post.author_id', \Request::get('author'))
        //         ->select('tbl_post.*')
        //         ->paginate(10);


        // 2
        // $query = Post::where('tbl_post.published',1);
        // if (\Request::get('category_id')) {
        //   $query = $query->leftJoin('tbl_category', 'tbl_post.category_id', '=', 'tbl_category.id')
        //                 ->where('tbl_post.category_id', \Request::get('category_id'));
        // }
        // if (\Request::get('author_id')) {
        //   $query = $query->leftJoin('users', 'tbl_post.author_id', '=', 'users.id')
        //                 ->where('tbl_post.author_id', \Request::get('author_id'));
        // }
        // if (\Request::get('title')) {
        //   $query = $query->where('tbl_post.title', 'like', \Request::get('title').'%');;
        // }
        // $query = $query->addSelect('tbl_post.*');
        // $posts = $query->paginate(10);

    
        // 3
        $query = Post::where(Post::$table_name.'.published',1);
        $query = $query->whereNull('type');
        if (\Request::get('category_id')) {
          $query = $query->leftJoin(Category::$table_name, Post::$table_name.'.category_id', '=', 'tbl_category.id')
                        ->where(Post::$table_name.'.category_id', \Request::get('category_id'));
        }
        if (\Request::get('author_id')) {
          $query = $query->leftJoin(Category::$table_name, Post::$table_name.'.author_id', '=', 'users.id')
                        ->where(Post::$table_name.'.author_id', \Request::get('author_id'));
        }
        if (\Request::get('title')) {
          $query = $query->where(Post::$table_name.'.title', 'like', \Request::get('title').'%');;
        }
        $query = $query->addSelect(Post::$table_name.'.*');
        $query = $query->orderBy(Post::$table_name.'.id', 'desc');
        $posts = $query->paginate(10);


        // $quries = DB::getQueryLog();
        // print_r($quries);

        return View::make('cms::post.backend.index' , compact('posts', 'categories', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::where('published',1)->pluck('title', 'id')->toArray();  

        $post = new Post();
        Breadcrumbs::register('dashboard.post.create', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.post.index');
            $breadcrumbs->push('Create', route('dashboard.post.create'));
        });
        return View::make('cms::post.backend.create' , compact('post', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     

        // validate
        $rules =  array(
            'title' => 'required|max:255',
            'content' => 'required|max:5000',
            );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('error', 'Creation failed!');
            return Redirect::route('dashboard.post.create')->withErrors($validator->errors());
        } else {

            $post = new Post;

            $data = request()->all();
            $data['author_id'] = auth()->id();
            $data['slug'] = str_slug(Input::get('title'));
            Post::create($data);
            
            // redirect
            Session::flash('success', 'Successfully created!');
            return Redirect::route('dashboard.post.index');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        // get the post
        $post = Post::find($id);
        if(!$post){
            Session::flash('error', 'Post not found');
            return Redirect::route('dashboard.post.index');
        }

        $categories = Category::where('published',1)->pluck('title', 'id')->toArray();  

        Breadcrumbs::register('dashboard.post.edit', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.post.index');
            $breadcrumbs->push('Edit');
        });
        // show the edit form and pass the post
        return View::make('cms::post.backend.update' , compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        $rules =  [
        'title' => 'required|max:255',
        'content' => 'required|max:5000',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('error', 'Update failed!');
            return Redirect::route('dashboard.post.edit', $id)->withErrors($validator->errors());
        } else {

            // update
            $post = Post::find($id);
            $post->update($request->all());
            // dd($request->all());

            // redirect
            Session::flash('success', 'Successfully updated!');
            return Redirect::route('dashboard.post.edit', $id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $post = Post::find($id);
        $post->delete();

        // redirect
        Session::flash('success', 'Successfully deleted!');
        return Redirect::route('dashboard.post.index');
    }

    /**
     * Handle Method does not exist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::flash('error', 'Page not found!');
        return Redirect::route('dashboard.post.index');
    }

    /**
     * Handle Method does not exist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function copy($id){
        // get the post
        $post = Post::find($id);
        if(!$post){
            Session::flash('error', 'Post not found');
            return Redirect::route('dashboard.post.index');
        }

        Breadcrumbs::register('dashboard.post.edit', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.post.index');
            $breadcrumbs->push('Edit');
        });

        $post = $post->replicate(); //copy attributes
        $post->slug = rand(1,500); 
        $post->save(); 

        // redirect
        Session::flash('success', "Successfully Copied (with slug={$post->slug}) !");
        return Redirect::route('dashboard.post.index');

    }
}
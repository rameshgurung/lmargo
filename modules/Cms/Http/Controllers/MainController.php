<?php namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class MainController extends Controller {
	
	public function index()
	{
		return view('main::index');
	}
	
}
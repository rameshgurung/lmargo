<?php namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Modules\Cms\Entities\Category;
use Modules\Cms\Entities\Post;
use Illuminate\Foundation\Auth\User;

use Illuminate\Support\Facades\View;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Validator;
use DB;

use Breadcrumbs;
use Modules\Cms\Http\Controllers\DashboardController;

class BlogController extends DashboardController {
	

    public function __construct(){
        parent::__construct();
        // to solve php artisan route:list error
        // error : Breadcrumb name "dashboard.blog.index" has already been registered
        if (! Breadcrumbs::exists('dashboard.blog.index')) {    
            Breadcrumbs::register('dashboard.blog.index', function($breadcrumbs)
            {
                $breadcrumbs->parent('dashboard');
                $breadcrumbs->push('Blog', route('dashboard.blog.index'));
            });
        }
        DB::enableQueryLog();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::All();  

        $query = Post::where(Post::$table_name.'.published',1);
        $query = $query->whereIn('type', array_values(get_blog_types()));
        if (\Request::get('author_id')) {
          $query = $query->leftJoin(Category::$table_name, Post::$table_name.'.author_id', '=', 'users.id')
                        ->where(Post::$table_name.'.author_id', \Request::get('author_id'));
        }
        if (\Request::get('title')) {
          $query = $query->where(Post::$table_name.'.title', 'like', \Request::get('title').'%');;
        }
        $query = $query->addSelect(Post::$table_name.'.*');
        $query = $query->orderBy(Post::$table_name.'.id', 'desc');
        
        $blogs = $query->paginate(10);

        return View::make('cms::blog.backend.index' , compact('blogs', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $blog_types = get_blog_types();  

        $blog = new post();
        Breadcrumbs::register('dashboard.blog.create', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.blog.index');
            $breadcrumbs->push('Create', route('dashboard.blog.create'));
        });
        return View::make('cms::Blog.backend.create' , compact('blog', 'blog_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        // validate
        $rules =  array(
            'title' => 'required|max:255',
            'content' => 'required|max:5000',
            );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('error', 'Creation failed!');
            return Redirect::route('dashboard.blog.create')->withErrors($validator->errors());
        } else {

            $data = request()->all();

            $blog_category = get_blog_category();
            $data['category_id'] = $blog_category->id;

            $data['author_id'] = auth()->id();
            $data['slug'] = str_slug(Input::get('title'));

            $data = $this->update_blog_data($data);

            Post::create($data);
            
            // redirect
            Session::flash('success', 'Successfully created!');
            return Redirect::route('dashboard.blog.index');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $blog_types = get_blog_types();  

        // get the Blog
        $blog = Post::find($id);
        if(!$blog){
            Session::flash('error', 'Blog not found');
            return Redirect::route('dashboard.blog.index');
        }

        Breadcrumbs::register('dashboard.blog.edit', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.blog.index');
            $breadcrumbs->push('Edit');
        });

        if($blog->type == get_blog_types('GALLERY')){
          // show the edit form and pass the Blog
          $images = json_encode(unserialize($blog->images));          
        }

        return View::make('cms::Blog.backend.update' , compact('blog', 'blog_types', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        $rules =  [
            'title' => 'required|max:255',
            'content' => 'required|max:5000',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('error', 'Update failed!');
            return Redirect::route('dashboard.blog.edit', $id)->withErrors($validator->errors());
        } else {

            // update
            $blog = Post::find($id);
            $data = request()->all();
            
            $data = $this->update_blog_data($data);
            
            $blog->update($data);

            // redirect
            Session::flash('success', 'Successfully updated!');
            return Redirect::route('dashboard.blog.edit', $id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $blog = Post::find($id);
        $blog->delete();

        // redirect
        Session::flash('success', 'Successfully deleted!');
        return Redirect::route('dashboard.blog.index');
    }

    /**
     * Handle Method does not exist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function copy($id){
        // get the Blog
        $blog = Post::find($id);
        if(!$blog){
            Session::flash('error', 'Blog not found');
            return Redirect::route('dashboard.blog.index');
        }

        Breadcrumbs::register('dashboard.blog.edit', function($breadcrumbs)
        {
            $breadcrumbs->parent('dashboard.blog.index');
            $breadcrumbs->push('Edit');
        });

        $blog = $blog->replicate(); //copy attributes
        $blog->slug = rand(1,500); 
        $blog->save(); 

        // redirect
        Session::flash('success', "Successfully Copied (with slug={$blog->slug}) !");
        return Redirect::route('dashboard.blog.index');

    }

    /**
     * [update_blog_data as per blog type]
     * @param  array $data
     * @return array      
     */
    private function update_blog_data($data){

      if($data['type'] == get_blog_types('IMAGE')){ 
        $data['video'] = $data['images'] = '';              
      }elseif($data['type'] == get_blog_types('VIDEO')){               
        $data['featured_image'] = $data['images'] = '';              
      }elseif($data['type'] == get_blog_types('GALLERY')){               
        $data['featured_image'] = $data['video'] = '';
        if(isset($data['images'])){
          $data['images'] = serialize($data['images']);
        }              
      }

      return $data;
    }
}
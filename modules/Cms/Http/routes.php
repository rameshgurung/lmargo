<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'dashboard', 'namespace' => 'Modules\Cms\Http\Controllers'], function()
{
	
	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::post('/', 'DashboardController@settingUpdate')->name('dashboard.setting.update');

	// Route::get('/category', ['uses' => 'CategoryController@index', 'as' => 'dashboard.category.index']);
	// Route::get('/category/create', ['uses' => 'CategoryController@create', 'as' => 'dashboard.category.create']);
	// Route::get('/category/edit', ['uses' => 'CategoryController@edit', 'as' => 'dashboard.category.edit']);
	// Route::get('/category/destroy', ['uses' => 'CategoryController@destroy', 'as' => 'dashboard.category.destroy']);
	
	Route::resource('category', 'CategoryController', ['except' => ['show']]);

	Route::get('post/{post}/copy', 'PostController@copy')->name('dashboard.post.copy');
	Route::resource('post', 'PostController', ['except' => ['show']]);

	Route::get('blog/{blog}/copy', 'BlogController@copy')->name('dashboard.blog.copy');
	Route::resource('blog', 'BlogController', ['except' => ['show']]);

	Route::get('menu/order', 'MenuController@order')->name('dashboard.menu.order');
	Route::get('menu/footer', 'MenuController@footerindex')->name('dashboard.menu.footer.index');
	Route::post('menu/footer', 'MenuController@footerstore')->name('dashboard.menu.footer.store');
	Route::get('menu/footer/create', 'MenuController@footercreate')->name('dashboard.menu.footer.create');
	Route::resource('menu', 'MenuController', ['except' => ['show']]);

});

<?php namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Cms\Entities\Post;

class Category extends Model {

     /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table = 'tbl_category';

     protected $fillable = [];

     protected static $rules = array(
     	'title' => 'required|max:255',
     	'content' => 'required|max:5000',
     	);

     // solve table name hardcode issue
     public static $table_name = 'tbl_category';

     public static function blogCategoryTitle(){
          return 'BLOG';
     }

     public function getValidationRules($mode = null){

     	if($mode){
     		switch ($mode) {
     			case 'C':{     				
     				break;
     			}     			
     			case 'U':{
     				break;
     			}     			
     			default:{
     				break;
     			}
     		}
     	}else{
     		return self::$rules;
     	}
     	
     }

     public function posts(){
        return $this->hasMany(Post::class);
     }     
 }
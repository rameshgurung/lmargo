<?php namespace Modules\Cms\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Modules\Cms\Entities\Category;
use Modules\Cms\Entities\Post;
use Illuminate\Foundation\Auth\User;


class Menu extends Model {

    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'tbl_menu';

    // solve table name hardcode issue
    public static $table_name = 'tbl_menu';


    protected $fillable = ['title', 'slug', 'url', 'page_type', 'menu_type', 'breadcrumb', 'breadcrumb_desc', 'published', 'level', 'order', 'desc', 'sidebar', 'custom_design', 'category_id', 'author_id', 'post_id', 'parent_menu_id'];

    const UPPER_MENU = 'UPPER';
    const FOOTER_MENU = 'FOOTER';

    const HOME = 'HOME';
    const CATEGORY = 'CATEGORY';
    const POST = 'POST';
    const CONTACT_US = 'CONTACT_US' ;       
    const BLOG = 'BLOG';
    const BLANK = 'BLANK';
    const CUSTOM = 'CUSTOM';
    const NOTFOUND = 'NOTFOUND';
    const TIMELINE = 'TIMELINE' ;       

    public static function get_menu_page_types($key=null){
         $types = array(
            self::HOME => 'HOME',
            self::CATEGORY => 'CATEGORY',
            self::POST => 'POST',
            self::CONTACT_US => 'CONTACT_US',
            self::BLOG => 'BLOG',
            self::BLANK => 'BLANK',
            self::CUSTOM => 'CUSTOM',
            self::NOTFOUND => 'NOTFOUND',
            self::TIMELINE => 'TIMELINE'
         );
         if($key && isset($key)) return $types[$key];
         return $types;
    }

    public static function get_menu_types($key=null){
        $types = array(
           self::UPPER_MENU => 'UPPER_MENU',
           self::FOOTER_MENU => 'FOOTER_MENU'
        );
        if($key && isset($key)) return $types[$key];
        return $types;
   }

    public function category()
	{
		return $this->belongsTo(Category::class);
	}    

    public function post()
	{
		return $this->belongsTo(Post::class);
	}    

    public function author()
    {
        return $this->belongsTo(User::class);
    }   

    public function parent_menu()
    {
        return $this->belongsTo(Menu::class);
    } 

    public function children_menus()
    {
        return $this->hasMany(Menu::class, 'parent_menu_id');
    }    

}
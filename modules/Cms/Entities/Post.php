<?php namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Cms\Entities\Category;
use Illuminate\Foundation\Auth\User;

class Post extends Model {

     /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table = 'tbl_post';

     protected $fillable = ['title', 'slug', 'url', 'content', 'excerpt', 'published', 'type', 'format', 'created_at', 'updated_at', 'deleted_at', 'category_id', 'author_id', 'order', 'featured_image', 'featured_image_title', 'images', 'video', 'video_title', 'video_url', 'video_embed', 'meta_key', 'meta_desc', 'meta_robot'];

     // solve table name hardcode issue
     public static $table_name = 'tbl_post';

     const IMAGE = 'IMAGE';
     const VIDEO = 'VIDEO';
     const GALLERY = 'GALLERY';

     public static function post_types($key=null){
          $types = array(
               self::IMAGE => 'IMAGE',
               self::VIDEO => 'VIDEO',
               self::GALLERY => 'GALLERY',
               );
          if($key && isset($key)) return $types[$key];
          return $types;
     }

	public function category()
	{
		return $this->belongsTo(Category::class);
	}    

     public function author()
     {
          return $this->belongsTo(User::class);
     }    

 }
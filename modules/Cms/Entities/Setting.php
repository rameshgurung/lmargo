<?php namespace Modules\Cms\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	/**
	* The table associated with the model.
	*
	* @var string
	*/
	protected $table = 'tbl_setting';

    protected $fillable = [];

}
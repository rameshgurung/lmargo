<!-- <h2 class="page-header">Settings</h2> -->

<div class="nav-tabs-custom">

	{{ Form::open(array('route' => 'dashboard.setting.update', 'method' => 'post', 'class' => 'form-horizontal', 'name' => 'form_options', 'enctype' => 'multipart/form-data' )) }}

	<!-- nav-tabs -->
	<ul class="nav nav-tabs">

		<li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">Home</a></li>
		<li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Footer Message</a></li>
		<li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">Sidebar</a></li>
		<li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false">Contact Settings</a></li>
		<li class=""><a data-toggle="tab" href="#tab-5" aria-expanded="false">Maintenance</a></li>

	</ul>
	<!-- nav-tabs -->

	<!-- tab-content -->
	<div class="tab-content">

		<!-- tab-1 -->
		<div class="tab-pane active" id="tab-1">

				<div class="row">

					<!-- title -->
					<div class="col-md-6">                  
						<h4 class="text-thin mar-btm">Title</h4><hr>

						<div class="form-group">
							<label class="col-md-3 control-label">Select Logo</label>

							<div class="col-md-9">
								<div class="pull-left btn btn-default">
									<a id="lfm-image-1" data-input="thumbnail-image-1" data-preview="holder-image-1" class="btn btn-primary">
										<i class="fa fa-picture-o"></i> Choose
									</a>
									<input class="form-control" id="thumbnail-image-1" name="header_logo" type="hidden" value="<?php echo $settings['header_logo'];?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-5">
								<ul>
									<li><b>Type: </b> gif | jpg | png </li>
									<li><b>Size: </b> < 1mb </li>
									<li><b>Dimension: </b> 1024 X 768 </li>
								</ul>
							</div>
							<div class="col-md-4">

								<?php 
								$img = image_exists($settings['header_logo']);
								?>
								<a href="<?php echo $img?>">
									<img id="holder-image-1" style="margin-top:15px;max-height:100px;" src="<?php echo $img;?>">
								</a>

							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Site Title</label>
							<div class="col-sm-9">
								<input name="site_name" type="text" placeholder="Club Name" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['site_name']?>" >
							</div>
						</div>
					</div>
					<!-- title -->

					<!-- Purchase -->
					<div  class="col-md-6">
						<h4 class="text-thin mar-btm">Purchase</h4><hr>

						<div class="form-group">
							<label class="col-md-3 control-label">Background</label>
							<div class="col-md-9">
								<div class="pull-left btn btn-default">

									<a id="lfm-image-2" data-input="thumbnail-image-2" data-preview="holder-image-2" class="btn btn-primary">
										<i class="fa fa-picture-o"></i> Choose
									</a>
									<input class="form-control" id="thumbnail-image-2" name="purchase_bg" type="hidden" value="<?php echo $settings['purchase_bg'];?>">

								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-5">
								<ul>
									<li><b>Type: </b> gif | jpg | png </li>
									<li><b>Size: </b> < 1mb </li>
									<li><b>Dimension: </b> 1024 X 768 </li>
								</ul>
							</div>
							<div class="col-md-4">

								<?php 
								$img = image_exists($settings['purchase_bg']);
								?>
								<a href="<?php echo $img?>">
									<img id="holder-image-2" style="margin-top:15px;max-height:100px;" src="<?php echo $img;?>">
								</a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Texts</label>
							<div class="col-sm-9">
								<input name="purchase_texts" type="text" placeholder="Slogan head" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['purchase_texts']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Line 1</label>
							<div class="col-sm-9">
								<input name="purchase_line1" type="text" placeholder="Slogan body" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['purchase_line1']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Line 2</label>
							<div class="col-sm-9">
								<input name="purchase_line2" type="text" placeholder="Dist." id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['purchase_line2']?>" >
							</div>
						</div>
					</div>
					<!-- Purchase -->

					<!-- Categories -->	
					<div class="col-md-12">
						<h4 class="text-thin mar-btm">Categories</h4><hr>						
					</div>				

					<!-- service -->
					<div  class="col-md-6">

						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Service</label>
							<div class="col-sm-9">
								<select name="services_category_id" id="" class="form-control">	

									<option value="">Select</option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" 
											@if ( $settings['services_category_id'] == $category->id ) 
												selected 
											@endif
											/>
											{{ $category->title }}
										</option>
									} ?>
									@endforeach

								</select>
							</div>
						</div>
					</div>					
					<!-- service -->

					<!-- work -->
					<div  class="col-md-6">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Work</label>
							<div class="col-sm-9">
								<select name="works_category_id" id="" class="form-control">	

									<option value="">Select</option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" 
											@if ( $settings['works_category_id'] == $category->id ) 
												selected 
											@endif
											/>
											{{ $category->title }}
										</option>
									} ?>
									@endforeach

								</select>
							</div>
						</div>
					</div>					
					<!-- work -->

					<!-- testimonial -->
					<div  class="col-md-6">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Testimonial</label>
							<div class="col-sm-9">
								<select name="testimonials_category_id" id="" class="form-control">	

									<option value="">Select</option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" 
											@if ( $settings['testimonials_category_id'] == $category->id ) 
												selected 
											@endif
											/>
											{{ $category->title }}
										</option>
									} ?>
									@endforeach

								</select>
							</div>
						</div>
					</div>					
					<!-- testimonial -->


					<!-- news -->
					<div  class="col-md-6">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">News</label>
							<div class="col-sm-9">
								<select name="news_category_id" id="" class="form-control">	

									<option value="">Select</option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" 
											@if ( $settings['news_category_id'] == $category->id ) 
												selected 
											@endif
											/>
											{{ $category->title }}
										</option>
									} ?>
									@endforeach

								</select>
							</div>
						</div>
					</div>					
					<!-- news -->


					<!-- Title -->


					<!-- save and cancel -->
					<div class="col-md-12"> 
						<hr>               
						<input name="header" class="btn btn-primary" type="submit" value="Save">
						<button class="btn btn-default" type="reset">Reset</button>
					</div>
				</div>
				<!-- save and cancel -->
			<!-- form -->
		</div>
		<!-- tab-1 -->

		<!-- tab-2 -->
		<div class="tab-pane" id="tab-2">
			
				<div class="row">                  
					<!-- mailchimp -->
					<div class="col-md-6">
						<h4 class="text-thin mar-btm">Mailchimp</h4><hr>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Get in touch</label>
							<div class="col-sm-9">
								<textarea name="footer_get_in_touch" id="" cols="30" rows="4" class="form-control" placeholder="sidebar 1 content"><?php echo $settings['footer_get_in_touch']?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">API KEY</label>
							<div class="col-sm-9">
								<input name="mailchimp_api_key" type="text" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['mailchimp_api_key']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">List ID</label>
							<div class="col-sm-9">
								<input name="mailchimp_api_list_id" type="text" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['mailchimp_api_list_id']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Campaign ID</label>
							<div class="col-sm-9">
								<input name="mailchimp_api_campaign_id" type="text" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['mailchimp_api_campaign_id']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Newsletter Category</label>
							<div class="col-sm-9">
								<select name="mailchimp_newsletter_category_id" id="" class="form-control">	

									<option value="">Select</option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" 
											@if ( $settings['mailchimp_newsletter_category_id'] == $category->id ) 
												selected 
											@endif
											/>
											{{ $category->title }}
										</option>
									} ?>
									@endforeach

								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Send Newsletter</label>
							<div class="col-sm-9">
								<div class="checkbox">
									<label><input type="checkbox" name="mailchimp_newsletter_enabled" 
										<?php 
										$enabled = $settings['mailchimp_newsletter_enabled'];
										if($enabled == 1){
											echo "checked='true'";
										}
										?>">
										Yes
									</label>                  
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Newsletter Footer</label>
							<div class="col-sm-9">
								<textarea name="mailchimp_newsletter_footer_msg" id="" cols="30" rows="3" class="form-control" placeholder="mailchimp_newsletter_footer_msg content"><?php echo $settings['mailchimp_newsletter_footer_msg']?></textarea>
							</div>
						</div>
					</div> 
					<!-- mailchimp -->

					<!-- margo -->
					<div class="col-md-6">
						<div class="col-md-12">
							<h4 class="text-thin mar-btm">Margo</h4><hr>
							<div class="box-body no-padding">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="demo-hor-inputemail">Introduction</label>
									<div class="col-sm-9">
										<textarea name="introduction" id="" cols="30" rows="10" class="form-control" placeholder="sidebar 1 content"><?php echo $settings['introduction']?></textarea>
									</div>
								</div>						
							</div> 
						</div>
						<!-- margo -->

						<!-- Copyright -->
						<div class="col-md-12">
							<h4 class="text-thin mar-btm">Copyright</h4><hr>
							<div class="form-group">
								<div class="col-sm-12">
									<input name="footer_copyright_message" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['footer_copyright_message']?>" >
								</div>
							</div>
						</div>
					</div>
					<!-- Copyright -->

					<!-- Twitter -->
					<div class="col-md-12">
						<div class="col-md-6">
							<h4 class="text-thin mar-btm">Twitter</h4><hr>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="demo-hor-inputpass">No of Twitter feed to show</label>
								<div class="col-sm-9">
									<input name="twitter_total_tweets" type="number" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['twitter_total_tweets']?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="demo-hor-inputpass">consumer_key</label>
								<div class="col-sm-9">
									<input name="twitter_consumer_key" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['twitter_consumer_key']?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="demo-hor-inputpass">consumer_secret</label>
								<div class="col-sm-9">
									<input name="twitter_consumer_secret" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['twitter_consumer_secret']?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="demo-hor-inputpass">access_token</label>
								<div class="col-sm-9">
									<input name="twitter_access_token" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['twitter_access_token']?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="demo-hor-inputpass">access_token_secret</label>
								<div class="col-sm-8">
									<input name="twitter_access_token_secret" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['twitter_access_token_secret']?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="demo-hor-inputpass">twitter_screen_name</label>
								<div class="col-sm-8">
									<input name="twitter_screen_name" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['twitter_screen_name']?>" >
								</div>
							</div>

						</div>
						<!-- twitter -->


						<!-- menu -->
						<div class="col-md-6">

							<h4 class="text-thin mar-btm">Menu</h4><hr>

							<div class="form-group">
								<label class="col-sm-1 control-label" for="demo-hor-inputpass">Title</label>
								<div class="col-sm-4">
									<input name="footer_custom_menu_item_title1" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['footer_custom_menu_item_title1']?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label" for="demo-hor-inputpass">Title</label>
								<div class="col-sm-4">
									<input name="footer_custom_menu_item_title2" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['footer_custom_menu_item_title2']?>" >
									<input name="footer_custom_menu_item_link_type2" type="hidden"
									value="<?php echo $settings['footer_custom_menu_item_link_type2']?>" >
								</div>
								<label class="col-sm-1 control-label" for="demo-hor-inputpass">Link</label>
								<div class="col-sm-3">
									<select name="footer_custom_menu_item_link2" id="" class="form-control">	

										<option value="">Select</option>
										@foreach($posts as $post)
											<option value="{{ $post->id }}" 
												@if ( $settings['footer_custom_menu_item_link2'] == $post->id ) 
													selected 
												@endif
												/>
												{{ $post->title }}
											</option>
										} ?>
										@endforeach

									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label" for="demo-hor-inputpass">Title</label>
								<div class="col-sm-4">
									<input name="footer_custom_menu_item_title3" type="text" id="demo-hor-inputpass" class="form-control"
									value="<?php echo $settings['footer_custom_menu_item_title3']?>" >
									<input name="footer_custom_menu_item_link_type3" type="hidden"
									value="<?php echo $settings['footer_custom_menu_item_link_type3']?>" >
								</div>
								<label class="col-sm-1 control-label" for="demo-hor-inputpass">Link</label>
								<div class="col-sm-3">
									<select name="footer_custom_menu_item_link3" id="" class="form-control">	

										<option value="">Select</option>
										<?php /*
										@foreach($menus as $menu)
											<option value="{{ $menu->id }}" 
												@if ( $settings['footer_custom_menu_item_link3'] == $menu->id ) 
													selected 
												@endif
												/>
												{{ $menu->title }}
											</option>
										} ?>
										@endforeach
										*/ ?>

									</select>
								</div>
							</div>
						</div>						
						<!-- menu -->
					</div>						

					<!-- save and cancel -->
					<div class="col-md-12">
						<hr>
						<input name="footer" class="btn btn-primary" type="submit" value="Save">
						<button class="btn btn-default" type="reset">Reset</button>
					</div>
					<!-- save and cancel -->
				</div>
		</div>
		<!-- tab-2 -->

		<!-- tab-3 -->
		<div class="tab-pane" id="tab-3">
				<div class="row">

					<div class="col-md-12">

						<h4 class="text-thin mar-btm">Widgets</h4><hr>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Names</label>
							<div class="col-sm-9">
								<input name="sidebar_widgets" type="text" placeholder="widget1, widget2" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['sidebar_widgets']?>" >
							<br>
							<i>Use widget shortcodes</i> (eg. widget1, widget2) 
							</div>
						</div>
						<!-- save and cancel -->
						<div class="col-md-12">
							<hr>                
							<input name="sidebar" class="btn btn-primary" type="submit" value="Save">
							<button class="btn btn-default" type="reset">Reset</button>
						</div>
						<!-- save and cancel -->
					</div>
			</div>
		</div>
		<!-- tab-3 -->

		<!-- tab-4 -->
		<div class="tab-pane" id="tab-4">
				<!-- Other sites -->
				<div class="row">

					<!-- contacts -->
					<div class="col-md-12">
						<h4 class="text-thin mar-btm">Contacts</h4><hr>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Phone Number</label>
							<div class="col-sm-4">
								<input name="telephone_1" type="text" placeholder="Phone 1" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['telephone_1']?>" >
							</div>
							<div class="col-sm-5">
								<input name="telephone_2" type="text" placeholder="Phone 2" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['telephone_2']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Mobiles</label>
							<div class="col-sm-4">
								<input name="mobile_1" type="text" placeholder="Mobile 1" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['mobile_1']?>" >
							</div>
							<div class="col-sm-5">
								<input name="mobile_2" type="text" placeholder="Mobile 2" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['mobile_2']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Emails</label>
							<div class="col-sm-4">
								<input name="email_1" type="text" placeholder="Email 1" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['email_1']?>" >
							</div>
							<div class="col-sm-5">
								<input name="email_2" type="text" placeholder="Email 2" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['email_2']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Website</label>
							<div class="col-sm-9">
								<input name="website" type="text" placeholder="website" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['website']?>" >
							</div>
						</div>
					</div>
					<!-- contacts -->

					<!-- address & working hours -->
					<div class="col-md-12">						
						<!-- Title -->
						<div class="col-md-4">                  
							<h4 class="text-thin mar-btm">Address1</h4><hr>
							<div class="box-body no-padding">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea name="location" id="" cols="30" rows="5" class="form-control" placeholder="sidebar 1 content"><?php echo $settings['location']?></textarea>
									</div>
								</div>
							</div> 
						</div>
						<div class="col-md-4">                  
							<h4 class="text-thin mar-btm">Address2</h4><hr>
							<div class="box-body no-padding">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea name="address2" id="" cols="30" rows="5" class="form-control" placeholder="sidebar 1 content"><?php echo $settings['address2']?></textarea>
									</div>
								</div>
							</div> 
						</div>
						<div class="col-md-4">                  
							<h4 class="text-thin mar-btm">Working Hours</h4><hr>
							<div class="box-body no-padding">
								<div class="form-group">
									<label class="col-sm-6 control-label" for="demo-hor-inputemail">Monday - Friday</label>
									<div class="col-sm-6">
										<input name="contact_working_hr1" type="text" placeholder="9am to 5pm" id="demo-hor-inputemail" class="form-control"
										value="<?php echo $settings['contact_working_hr1']?>" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-6 control-label" for="demo-hor-inputemail">Saturday</label>
									<div class="col-sm-6">
										<input name="contact_working_hr2" type="text" placeholder="9am to 5pm" id="demo-hor-inputemail" class="form-control"
										value="<?php echo $settings['contact_working_hr2']?>" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-6 control-label" for="demo-hor-inputemail">Sunday</label>
									<div class="col-sm-6">
										<input name="contact_working_hr3" type="text" placeholder="9am to 5pm" id="demo-hor-inputemail" class="form-control"
										value="<?php echo $settings['contact_working_hr3']?>" >
									</div>
								</div>
							</div> 
						</div>
						<!-- Title -->
					</div>
					<!-- address & working hours -->

					<!-- social links -->
					<div  class="col-md-6">
						<h4 class="text-thin mar-btm">Social Links</h4><hr>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Facebook</label>
							<div class="col-sm-9">
								<input name="facebook_link" type="text" placeholder="Slogan body" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['facebook_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Twitter</label>
							<div class="col-sm-9">
								<input name="twitter_link" type="text" placeholder="Dist." id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['twitter_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Google Plus</label>
							<div class="col-sm-9">
								<input name="google_plus_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['google_plus_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Dribble</label>
							<div class="col-sm-9">
								<input name="dribble_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['dribble_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Linkedin</label>
							<div class="col-sm-9">
								<input name="linkedin_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['linkedin_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Google Plus</label>
							<div class="col-sm-9">
								<input name="google_plus_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['google_plus_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Flickr</label>
							<div class="col-sm-9">
								<input name="flickr_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['flickr_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Tumblr</label>
							<div class="col-sm-9">
								<input name="tumblr_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['tumblr_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Instagram</label>
							<div class="col-sm-9">
								<input name="instagram_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['instagram_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Vimeo</label>
							<div class="col-sm-9">
								<input name="vimeo_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['vimeo_link']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputpass">Skype</label>
							<div class="col-sm-9">
								<input name="skype_link" type="text" placeholder="" id="demo-hor-inputpass" class="form-control"
								value="<?php echo $settings['skype_link']?>" >
							</div>
						</div>
					</div>

					<!-- info -->
					<div class="col-md-6">                  
						<h4 class="text-thin mar-btm">Information</h4><hr>
						<div class="form-group">
							<div class="col-sm-12">
								<textarea name="contact_information" id="" cols="30" rows="5" class="form-control" placeholder="contact information"><?php echo $settings['contact_information']?></textarea>
							</div>
						</div> 
					</div>
					<!-- info -->

					<!-- save and cancel -->
					<div class="col-md-12"> 
						<hr>               
						<input name="contact" class="btn btn-primary" type="submit" value="Save">
						<!-- <button class="btn btn-default" type="reset">Reset</button> -->
						<button class="btn btn-default" type="reset">Reset</button>
					</div>
					<!-- save and cancel -->

				</div>
		</div>
		<!-- tab-4 -->

		<!-- tab-5 -->
		<div class="tab-pane" id="tab-5">
				<div class="row">

					<div class="col-md-12">

						<h4 class="text-thin mar-btm">Options</h4><hr>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Title</label>
							<div class="col-sm-4">
								<input name="maintenance_title" type="text" placeholder="Title Here" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['maintenance_title']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Message</label>
							<div class="col-sm-9">
								<input name="maintenance_msg" type="text" placeholder="Message Here" id="demo-hor-inputemail" class="form-control"
								value="<?php echo $settings['maintenance_msg']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="demo-hor-inputemail">Enable Mode</label>
							<div class="col-sm-9">
								<div class="checkbox">
									<label><input type="checkbox" name="maintenance_mode_enabled" 
										<?php 
										$enabled = $settings['maintenance_mode_enabled'];
										if($enabled == 1){
											echo "checked='true'";
										}
										?>">Yes</label>                  
									</div>
								</div>
							</div>
						</div>
						<!-- save and cancel -->
						<div class="col-md-12">
							<hr>                
							<input name="maintenance" class="btn btn-primary" type="submit" value="Save">
							<button class="btn btn-default" type="reset">Reset</button>
						</div>
						<!-- save and cancel -->
					</div>
			</div>
		</div>
		<!-- tab-5 -->

	</div>
	<!-- tab-content -->

	{{ Form::close() }}

	<style>
		.tab-content {
			padding: 1%;
		}
		.nav.nav-tabs li a{
			color: black;
		}
		#no-color{
			background-color: #E7EBEE !important;
			border-color: #E7EBEE !important;
		}
		.box-body no-padding{
			margin-top: 1% !important;
		}
		.mar-btm {
			margin-bottom: none !important;
		}
		.box-body no-padding {
			padding: none !important;
		}
		.nav-tabs{
			background-color: #ddd;
		}
	</style>

	<script>
		$(function(){

			var tab = '<?php echo $active_setting_tab;?>';
			$('.nav-tabs a[href="#tab-' + tab + '"]').tab('show');

			$('#lfm-image-1').filemanager('image', {prefix: "{{ route( 'dashboard.laravelfilemanager' ) }}"});
			$('#lfm-image-2').filemanager('image', {prefix: "{{ route( 'dashboard.laravelfilemanager' ) }}"});


		});
	</script>



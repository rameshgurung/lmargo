@extends('backend.layout')

@section('title', 'Categories')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			{{ Form::model($category, array('route' => array('dashboard.category.update', $category->id) , 'method' => 'PUT')) }}    

			<div class="box-header with-border">
				<h3 class="box-title">Update Category</h3>				
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				


				<div class="form-group">
					<!-- title -->
					{{ Form::label('title', 'Title') }}
					{{ Form::text('title', null , array( 'class' => 'form-control' ) ) }}      
				</div>

				<div class="form-group">
					<!-- content -->
					{{ Form::label('content', 'Content') }}
					{{ Form::textarea('content', null , array( 'class' => 'form-control' ) ) }}      
				</div>


			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
				{{ Form::submit('Update', array( 'class' => 'btn btn-primary' )) }}

				{{ Form::close() }}
				
			</div>
		</div>
		<!-- /.box -->
	</div>
</div>


@endsection




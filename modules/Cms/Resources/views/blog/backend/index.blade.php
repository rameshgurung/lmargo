@extends('backend.layout')

@section('title', 'Blogs')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Blogs</h3>
				<span class="pull-right">
					<a href="{{ route('dashboard.blog.create') }}" title="Create">
						<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
					</a>
				</span>

				<form action="" method="GET" role="form">
					<table class="table filter">            
						<tbody>
							<tr>
								<td class="col-lg-3">
									<div class="form-group">
										<label>Author</label>
										<select id="author_id" class="form-control" name="author_id">
											<option value="">Select</option>
											@foreach($users as $user)
												<option value="{{ $user->id }}"
													@if ( \Request::get('author_id') == $user->id ) 
														selected 
													@endif
													/>
													{{ $user->name }}
												</option>
											} ?>
											@endforeach
										</select>
									</div>
								</td>
								<td class="col-lg-3">
									<div class="form-group">
										<label>Blog Title</label>
										<div class="controls">
											<input type="text" name="title" id="title"  data-required="1" 
											class="form-control" placeholder="Blog Title here"
											value="{{ \Request::get('title') }}">
										</div>
									</div>
								</td>
								<td class="col-lg-3">
									<div class="form-group pull-right">
										<br>
										<br>
										<div class="controls">
											<input type="submit" class="btn btn-info" name="filter" value="Filter">
											<a href=" {{ route( 'dashboard.blog.index' ) }}" class="btn btn-default">Reset</a>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</form>

			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 10px">#</th>
							<th>Title</th>
							<th>Type</th>
							<th>Created At</th>
							<th>Actions</th>
						</tr>

						@if( count($blogs) > 0 )
						@foreach($blogs as $blog)
						<tr>
							<td>{{ $blog->id }}</td>
							<td>
								{{ $blog->title }}
								<br>
								-- <i>by : {{ $blog->author->name }}</i> -- 
							</td>
							
							<td>
								<?php 
									if($blog->type){
										$html = '';
										switch ($blog->type) {
											case get_blog_types('IMAGE'):
												if($blog->featured_image){
													echo "<img src='".url($blog->featured_image)."' class='img-responsive' width='200px' height='150px' title='{$blog->featured_image_title}'>";
												}
												break;
											case get_blog_types('VIDEO'):
												if(trim($blog->video)){
													echo "<iframe src='".url($blog->video)."'  width='200px' height='150px' title='{$blog->video_title}'></iframe>";
												}elseif (trim($blog->video_url)) {
													?>
													<video width="320" height="240" style="margin-top:15px;max-height:100px;" controls>
													  <source src="<?php echo $blog->video_url ? : '';?>" type="video/mp4">
													  <source src="<?php echo $blog->video_url ? : '';?>" type="video/webm">
													  <source src="<?php echo $blog->video_url ? : '';?>" type="video/ogg">
													  Your browser does not support the video tag.
													</video>
													<?php
												}elseif (trim($blog->video_embed)) {
													echo $blog->video_embed;
												}
												break;
											case get_blog_types('GALLERY'):
												if($blog->images){
												  $images = $unserialized = unserialize($blog->images);
												  if($unserialized !== false && count($images)>0){
												    ?>
												    <div id="carousel-example-generic" class="carousel slide" style="max-width:200px; max-height:150px;" data-ride="carousel">

												        <ol class="carousel-indicators">
												            <?php foreach ($images as $key => $image) { ?>
												                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $key?>" class="<?php echo $key==0?'active':''?>"></li>
												                <?php 
												            } ?>
												        </ol>

												        <div class="carousel-inner">
												            <?php foreach ($images as $key => $image) { ?>
												                <div class="item <?php echo $key==0?'active':''?>">
												                    <img src="<?php echo url($image)?>">
												                </div>
												                <?php 
												            } ?>                                                    
												        </div>

												        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
												            <span class="fa fa-angle-left"></span>
												        </a>

												        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
												            <span class="fa fa-angle-right"></span>
												        </a>
												    </div>
												    <?php 
													}
												}																							
												break;											
										}
										echo "<br>";
										echo get_blog_types($blog->type); 
									}
								?>
							</td>
							<td>{{ $blog->created_at }}</td>
							<td>													

								<a href=" {{ route( 'dashboard.blog.edit' , $blog->id ) }} " class="btn btn-default fa fa-pencil" data-toggle="tooltip" data-original-title="Edit"></a>

								<a href=" {{ route( 'dashboard.blog.copy' , $blog->id ) }} " class="btn btn-sm btn-icon add-tooltip btn-default fa fa-copy" data-toggle="tooltip" data-original-title="Copy"></a>

								{!! Form::open(array(
									'url' => URL::route( 'dashboard.blog.destroy' , $blog->id),
									'class' => 'form-delete' 
									)
								) 
								!!}
								{!! Form::hidden('_method', 'DELETE') !!}
								<button type="submit" class="btn btn-sm btn-danger fa fa-times delete" data-toggle="tooltip" data-original-title="Delete">
								</button>
								{!! Form::close() !!}

							</td>
						</tr>
						@endforeach
						@else
						<tr>
							<td colspan="6" class="text-center">No data found</td>
						</tr>
						@endif

					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
				<div class="col-sm-8">
					@if( count($blogs) > 0 )
					Showing  {{ $blogs->firstItem() }} to {{ $blogs->lastItem() }} of {{ $blogs->total() }} entries
					@endif
				</div>

				<div class="col-sm-4">					
					<ul class="pagination pagination-sm no-margin pull-right">
						{{ $blogs->links() }}
					</ul>
				</div>

			</div>
		</div>
		<!-- /.box -->
	</div>
</div>

@endsection



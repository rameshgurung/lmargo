@extends('backend.layout')

@section('title', 'Edit Blog')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Blog</h3>				
			</div>
			<!-- /.box-header -->

			<div class="box-body no-padding">
				{{ Form::model($blog, array('route' => array('dashboard.blog.update', $blog->id) , 'method' => 'PUT')) }}    

					<span class="col-md-4 col-lg-4">
					<div class="form-group">
						<!-- title -->
						{{ Form::label('title', 'Title') }}
						{{ Form::text('title', null , array( 'class' => 'form-control' ) ) }}      
					</div>
					</span>

					<span class="col-md-4 col-lg-4">
					<div class="form-group">
						<!-- category -->						
						{{ Form::label('type', 'Type') }}
						{!! Form::select('type', $blog_types, null , array( 'class' => 'form-control', 'id' => 'blog_type' )) !!}
						</div>					
					</span>

					<span class="col-md-4 col-lg-4">
					<div class="form-group">
						<!-- url -->
						{{ Form::label('url', 'Url') }}
						{{ Form::text('url', null , array( 'class' => 'form-control', 'placeholder' => '/aboutus' ) ) }}      
					</div>					
					</span>

					<span class="col-md-12 col-lg-12">
					<div class="form-group">
						<!-- content -->
						{{ Form::label('content', 'Content') }}
						{{ Form::textarea('content', null, array( 'class' => 'form-control', 'id' => 'mytextarea' ) ) }}
					</div>
					</span>

				<span class="col-md-6 col-lg-6">

					<p>
					  <h5>Meta Info.</h5> 
					  <div class="form-group">
						<!-- meta_desc -->
						{{ Form::label('meta_desc', 'Meta description') }}
						{{ Form::text('meta_desc', null, array( 'class' => 'form-control' ) ) }}
					  </div> 
					  <div class="form-group">
						<!-- meta_key -->
						{{ Form::label('meta_key', 'Meta keywords') }}
						{{ Form::text('meta_key', null, array( 'class' => 'form-control' ) ) }}
					  </div> 
					  <div class="form-group">
						<!-- meta_robot -->
						{{ Form::label('meta_robot', 'Meta robots') }}
						{{ Form::text('meta_robot', null, array( 'class' => 'form-control' ) ) }}
					  </div> 
					</p>
				</span>

				<span class="col-md-6 col-lg-6">
					
					<span class="blog_type_image">
					  <h5>Image Info.</h5>          
					  <div class="form-group">
						<!-- featured_image_title -->
						{{ Form::label('featured_image_title', 'Image title') }}
						{{ Form::text('featured_image_title', null, array( 'class' => 'form-control' ) ) }}
					  </div> 
					  <div class="form-group">

						<!-- featured_image -->
						{{ Form::label('Image', 'Image') }}

						<div class="input-group">
						<span class="input-group-btn">
						 <a id="lfm-image" data-input="thumbnail-image" data-preview="holder-image" class="btn btn-primary">
						   <i class="fa fa-picture-o"></i> Choose
						 </a>
						</span>
						{{ Form::hidden('featured_image', null, array( 'class' => 'form-control', 'id' => 'thumbnail-image' ) ) }}
						</div>
						<img id="holder-image" style="margin-top:15px;max-height:100px;" 
						<?php echo $blog->featured_image ? "src='".url($blog->featured_image)."'": '';?>>
					  </div>
					</span>

					<span class="blog_type_video" hidden>
					  <h5>Video Info.</h5>          


						<div class="form-group">
						<!-- vdo_title -->
						{{ Form::label('video_title', 'Video title') }}
						{{ Form::text('video_title', null, array( 'class' => 'form-control' ) ) }}
						</div> 

					  <div class="form-group">

						<!-- featured_image -->
						{{ Form::label('Video', 'Video') }}

						<div class="input-group">
						<span class="input-group-btn">
						 <a id="lfm-video" data-input="thumbnail-video" data-preview="holder-video" class="btn btn-primary">
						   <i class="fa fa-picture-o"></i> Choose
						 </a>
						</span>
						{{ Form::hidden('video', null, array( 'class' => 'form-control', 'id' => 'thumbnail-video') ) }}
						</div>
						<iframe id="holder-video" style="margin-top:15px;max-height:100px;" 
						<?php echo $blog->video ? "src='".url($blog->video)."'": '';?> 
						frameborder="0" class="video_preview" width="320" height="240"></iframe>

					  </div>
					  	
					  <div class="form-group">
						OR 
						<!-- video_url -->
						{{ Form::label('video_url', 'Video url') }}
						{{ Form::text('video_url', null, array( 'class' => 'form-control' ) ) }}

						<video width="320" height="240" style="margin-top:15px;max-height:100px;" controls>
						  <source src="<?php echo $blog->video_url ? : '';?>" type="video/mp4">
						  <source src="<?php echo $blog->video_url ? : '';?>" type="video/webm">
						  <source src="<?php echo $blog->video_url ? : '';?>" type="video/ogg">
						  Your browser does not support the video tag.
						</video>

					  </div>
					  	
					  <div class="form-group">
						OR <!-- video_embed -->
						{{ Form::label('video_embed', 'Video Embed') }}
						{{ Form::textarea('video_embed', null, array( 'class' => 'form-control', 'rows' => '3') ) }}
					  </div>					  
					</span>   

					<span class="blog_type_gallery" hidden>
					  <h5>Image Info.</h5>          
					  <div class="form-group">
						<!-- featured_image_title -->
						{{ Form::label('featured_image_title', 'Image title') }}
						{{ Form::text('featured_image_title', null, array( 'class' => 'form-control' ) ) }}
					  </div> 
					  <div class="form-group">

						<!-- featured_image -->
						{{ Form::label('Gallery Image', 'Gallery Image') }}

						<div class="input-group">
						<span class="input-group-btn">
						 <a id="lfm-image-gallery" data-input="thumbnail-image" data-preview="holder-image-gallery" class="btn btn-primary">
						   <i class="fa fa-picture-o"></i> Choose More
						 </a>
						</span>
						</div>
						

					  </div>
					</span>
				</span>

			</div>

			<!-- /.box-body -->
			<div class="box-footer clearfix">
					{{ Form::submit('Save', array( 'class' => 'btn btn-primary' )) }}
					{{ Form::close() }}
					<a href=" {{ route( 'dashboard.blog.index' ) }}" class="btn btn-default"/>Cancel</a>				
			</div>

		</div>
		<!-- /.box -->
	</div>
</div>
<script type="text/javascript">
	var blog_edit_or_create = true;
	var blog_gallery_images = JSON.parse('<?php echo isset($images)?$images:'[]';?>');
</script>
@endsection
@extends('backend.layout')

@section('title', 'Menus')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Order Menu</h3>
				<span class="pull-right">
					<a href="{{ route('dashboard.category.create') }}" title="Create">
						<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
					</a>
				</span>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 10px">#</th>
							<th>Title</th>
							<th>Created At</th>
							<th>Actions</th>
						</tr>

						@if( count($menus) > 0 )
						@foreach($menus as $menu)
						<tr>
							<td>{{ $menu->id }}</td>
							<td>{{ $menu->title }}</td>
							<td>{{ $menu->created_at }}</td>
							<td>													

								<a href=" {{ route( 'dashboard.category.edit' , $menu->id ) }} " class="btn btn-default fa fa-pencil" data-toggle="tooltip" data-original-title="Edit"></a>

								{!! Form::open(array(
									'url' => URL::route( 'dashboard.category.destroy' , $menu->id),
									'class' => 'form-delete' 
									)
								) 
								!!}
								{!! Form::hidden('_method', 'DELETE') !!}
								<button type="submit" class="btn btn-sm btn-danger fa fa-times delete" data-toggle="tooltip" data-original-title="Delete">
								</button>
								{!! Form::close() !!}

							</td>
						</tr>
						@endforeach
						@else
						<tr>
							<td colspan="4" class="text-center">No entries found</td>
						</tr>
						@endif

					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

@endsection



@extends('backend.layout')

@section('title', 'Menus')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Menus</h3>
				<span class="pull-right">
					<a href="{{ route('dashboard.menu.create') }}" title="Create">
						<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
					</a>
					<a href="{{ route('dashboard.menu.order') }}" class="btn btn-default"/>Order Menu</a>
				</span>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 10px">#</th>
							<th>Title</th>
							<th>Type</th>
							<th>URL</th>
							<th>Breadcrumb</th>
							<th>Sidebar</th>
							<th>Active</th>
							<th>Created At</th>
							<th>Actions</th>
						</tr>

						@if( count($menus) > 0 )
						@foreach($menus as $menu)
						<tr>
							<td>{{ $menu->id }}</td>
							<td>{{ $menu->title }}</td>
							<td>
								{{ $menu->page_type }}								
								@if ($menu->page_type == 'CATEGORY' && $menu->category )
									- (<a href=" {{ route( 'dashboard.category.edit' , $menu->category->id ) }} ">{{ $menu->category->title }}</a>) 
								@elseif ($menu->page_type == 'POST' && $menu->post )
									- (<a href=" {{ route( 'dashboard.post.edit' , $menu->post->id ) }} ">{{ $menu->post->title }}</a>)
								@endif

							</td>
							<td><a href="{{ url($menu->url) }}">{{ $menu->url }}</a></td>
							<td>{{ $menu->breadcrumb }}</td>
							<td>{{ $menu->sidebar?"Yes":"No" }}</td>
							<td>{{ $menu->published?"Yes":"No" }}</td>
							<td>{{ $menu->created_at }}</td>
							<td>													

								<a href=" {{ route( 'dashboard.menu.edit' , $menu->id ) }} " class="btn btn-default fa fa-pencil" data-toggle="tooltip" data-original-title="Edit"></a>

								{!! Form::open(array(
									'url' => URL::route( 'dashboard.menu.destroy' , $menu->id),
									'class' => 'form-delete' 
									)
								) 
								!!}
								{!! Form::hidden('_method', 'DELETE') !!}
								<button type="submit" class="btn btn-sm btn-danger fa fa-times delete" data-toggle="tooltip" data-original-title="Delete">
								</button>
								{!! Form::close() !!}

							</td>
						</tr>

							@if ($menu->children_menus)
								@foreach ($menu->children_menus as $child_menu)
									@if ($child_menu->menu_type == 'UPPER')
										<tr>
											<td style="padding-left:35px"></td>
											<td style="padding-left:35px">{{ $child_menu->title }}</td>
											<td>
												{{ $child_menu->page_type }}								
												@if ($child_menu->page_type == 'CATEGORY' && $child_menu->category )
													- (<a href=" {{ route( 'dashboard.category.edit' , $child_menu->category->id ) }} ">{{ $child_menu->category->title }}</a>) 
												@elseif ($child_menu->page_type == 'POST' && $child_menu->post )
													- (<a href=" {{ route( 'dashboard.post.edit' , $child_menu->post->id ) }} ">{{ $child_menu->post->title }}</a>)
												@endif
											</td>
											<td><a href="{{ url($child_menu->url) }}">{{ $child_menu->url }}</a></td>
											<td>{{ $child_menu->breadcrumb }}</td>
											<td>{{ $child_menu->sidebar?"Yes":"No" }}</td>
											<td>{{ $child_menu->published?"Yes":"No" }}</td>
											<td>{{ $child_menu->created_at }}</td>
											<td>													

												<a href=" {{ route( 'dashboard.menu.edit' , $child_menu->id ) }} " class="btn btn-default fa fa-pencil" data-toggle="tooltip" data-original-title="Edit"></a>

												{!! Form::open(array(
													'url' => URL::route( 'dashboard.menu.destroy' , $child_menu->id),
													'class' => 'form-delete' 
													)
												) 
												!!}
												{!! Form::hidden('_method', 'DELETE') !!}
												<button type="submit" class="btn btn-sm btn-danger fa fa-times delete" data-toggle="tooltip" data-original-title="Delete">
												</button>
												{!! Form::close() !!}

											</td>
										</tr>
									@endif
								@endforeach
							@endif
						
						@endforeach
						@else
						<tr>
							<td colspan="4" class="text-center">No entries found</td>
						</tr>
						@endif

					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
			<?php /*
			<div class="box-footer clearfix">
				
				<div class="col-sm-8">
					@if( count($menus) > 0 )
					Showing  {{ $menus->firstItem() }} to {{ $menus->lastItem() }} of {{ $menus->total() }} entries
					@endif
				</div>

				<div class="col-sm-4">					
					<ul class="pagination pagination-sm no-margin pull-right">
						{{ $menus->links() }}
					</ul>
				</div>

			</div>
			*/
			?>
		</div>
		<!-- /.box -->
	</div>
</div>

@endsection



@extends('backend.layout')

@section('title', 'Create Footer Menu')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Create Footer Menu</h3>				
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				{{ Form::model($menu, array('route' => array('dashboard.menu.store'))) }}    

				<div class="form-group">
					<!-- title -->
					{{ Form::label('title', 'Title') }}
					{{ Form::text('title', '', array( 'class' => 'form-control' ) ) }}      
				</div>

				<div class="form-group">
					<!-- desc -->
					{{ Form::label('Description', 'Description') }}
					{{ Form::textarea('desc', '', array( 'class' => 'form-control' ) ) }}      
				</div>

					{{ Form::submit('Create', array( 'class' => 'btn btn-primary' )) }}

				{{ Form::close() }}
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
			</div>
		</div>
		<!-- /.box -->
	</div>
</div>


@endsection
@extends('backend.layout')

@section('title', 'Menu')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Create Menu</h3>
			</div>
			<!-- /.box-header -->
			{{ Form::model($menu, array('route' => array('dashboard.menu.store'))) }}
			<div class="box-body">

				<div class="row">
					<span class="col-md-3 col-lg-3">
						<div class="form-group">
							<!-- title -->
							{{ Form::label('title', 'Title') }}
							{!! Form::text('title', '', array( 'class' => 'form-control' ) ) !!}
						</div>
					</span>
					<span class="col-md-3 col-lg-3">
						<div class="form-group">
							<!-- url -->
							{{ Form::label('url', 'URL') }}
							{{ Form::text('url', '', array( 'class' => 'form-control' ) ) }}
						</div>
					</span>
					<span class="col-md-2 col-lg-2">
						<div class="form-group">
							<label for="parent_menu">Parent menu</label>

								<select name="parent_menu_id" id="" class="form-control">	

									<option value="">Please Select</option>
									@foreach($upper_parent_menus as $upper_parent_menu)
										<option value="{{ $upper_parent_menu->id }}">
											{{ $upper_parent_menu->title }}
										</option>
									} ?>
									@endforeach

								</select>

						</div>
					</span>
					<span class="col-md-2 col-lg-2">
						<div class="form-group">
							<label for="page_type">Menu page type</label>
							{!! Form::select('page_type', $menu_page_types, null , array( 'class' => 'form-control')) !!}
						</div>
					</span>
					<span class="col-md-2 col-lg-2">
						<div class="form-group">
							<label for="page_type">Menu Type</label>
							{!! Form::select('menu_type', $menu_types, null , array( 'class' => 'form-control')) !!}
						</div>
					</span>
				</div>

				<div class="row">
					<span class="col-md-3 col-lg-3">
						<div class="form-group">
							<label for="category">Category</label>
							{!! Form::select('category_id', $categories, null , array( 'class' => 'form-control', 'id' => 'category_id')) !!}
						</div>
					</span>
					<span class="col-md-3 col-lg-3">
						<label for="article">Post</label>
						{!! Form::select('post_id', $posts, null , array( 'class' => 'form-control', 'id' => 'post_id')) !!}
					</span>

					<span class="col-md-3 col-lg-3">
						<div class="form-group">
							<label for="sidebar">Include Sidbars</label>
							<br>
							{!! Form::checkbox('sidebar', 1, false); !!}
						</div>
					</span>

					<span class="col-md-3 col-lg-3">
						<div class="form-group">
							<label for="status">Active Status</label>
							<br>
							{!! Form::checkbox('published', 1, false); !!}
							</select>
						</div>
					</span>
				</div>

				<div class="row">
					<div class="col-md-6 col-lg-6">
						<div class="form-group">
							<!-- breadcrumb -->
							{{ Form::label('breadcrumb', 'Breadcrumb') }}
							{{ Form::text('breadcrumb', '', array( 'class' => 'form-control' ) ) }}
						</div>
					</div>
				</div>
				<div class="row">
					<span class="col-md-12 col-lg-12">
						<div class="form-group">
							<!-- custom_design -->
							{{ Form::label('custom_design', 'Custom Design') }}
							{{ Form::textarea('custom_design', '', array( 'class' => 'form-control tinymice' ) ) }}
						</div>
					</span>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				<!-- /.box-body -->
				{{ Form::submit('Save', array( 'class' => 'btn btn-primary' )) }}
				<a href=" {{ route( 'dashboard.menu.index' ) }}" class="btn btn-default" />Cancel</a>
			</div>
			{{ Form::close() }}
		</div>
		<!-- /.box -->
	</div>
</div>

<script>
	$(function() {
		$('input[name="name"]').keyup(function() {
			$('input[name="url"]').val(this.value);
		});
	})
</script>


@endsection
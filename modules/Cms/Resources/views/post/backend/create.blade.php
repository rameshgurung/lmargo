@extends('backend.layout')

@section('title', 'Create Post')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Create Post</h3>				
			</div>
			<!-- /.box-header -->

			<div class="box-body no-padding">

				{{ Form::model($post, array('route' => array('dashboard.post.store'))) }}       

					<span class="col-md-4 col-lg-4">
					<div class="form-group">
						<!-- title -->
						{{ Form::label('title', 'Title') }}
						{{ Form::text('title', null , array( 'class' => 'form-control' ) ) }}      
					</div>
					</span>

					<span class="col-md-4 col-lg-4">
					<div class="form-group">
						<!-- category -->						
						{{ Form::label('category', 'category') }}
						{!! Form::select('category_id', $categories, null , array( 'class' => 'form-control' )) !!}
						</div>					
					</span>

					<span class="col-md-4 col-lg-4">
					<div class="form-group">
						<!-- url -->
						{{ Form::label('url', 'Url') }}
						{{ Form::text('url', null , array( 'class' => 'form-control', 'placeholder' => '/aboutus' ) ) }}      
					</div>					
					</span>

					<span class="col-md-12 col-lg-12">
					<div class="form-group">
						<!-- content -->
						{{ Form::label('content', 'Content') }}
						{{ Form::textarea('content', null, array( 'class' => 'form-control', 'id' => 'mytextarea' ) ) }}
					</div>
					</span>

				<span class="col-md-6 col-lg-6">
				<p>
				  <h4>Feature Image Info.</h4>          
				  <div class="form-group">

					<!-- featured_image -->
					<!-- {{ Form::label('featured_image', 'Feature Image') }} -->

					<div class="input-group">
					<span class="input-group-btn">
					 <a id="lfm-image" data-input="thumbnail-image" data-preview="holder-image" class="btn btn-primary">
					   <i class="fa fa-picture-o"></i> Choose
					 </a>
					</span>
					{{ Form::hidden('featured_image', null, array( 'class' => 'form-control', 'id' => 'thumbnail-image' ) ) }}
					</div>
					<img id="holder-image" style="margin-top:15px;max-height:100px;" 
					<?php echo $post->featured_image ? "src='".url($post->featured_image)."'": '';?> 
					>

				  </div>
				  <div class="form-group">
					<!-- featured_image_title -->
					{{ Form::label('featured_image_title', 'Image title') }}
					{{ Form::text('featured_image_title', null, array( 'class' => 'form-control' ) ) }}
				  </div> 
				  <h5>Meta Info.</h5> 
				  <div class="form-group">
					<!-- meta_desc -->
					{{ Form::label('meta_desc', 'Meta description') }}
					{{ Form::text('meta_desc', null, array( 'class' => 'form-control' ) ) }}
				  </div> 
				  <div class="form-group">
					<!-- meta_key -->
					{{ Form::label('meta_key', 'Meta keywords') }}
					{{ Form::text('meta_key', null, array( 'class' => 'form-control' ) ) }}
				  </div> 
				  <div class="form-group">
					<!-- meta_robot -->
					{{ Form::label('meta_robot', 'Meta robots') }}
					{{ Form::text('meta_robot', null, array( 'class' => 'form-control' ) ) }}
				  </div> 
				</p>
				</span>
				<span class="col-md-6 col-lg-6">
				<p>
				  <h4>Feature Video Info.</h4>          

				  <div class="form-group">

					<div class="input-group">
					<span class="input-group-btn">
					 <a id="lfm-video" data-input="thumbnail-video" data-preview="holder-video" class="btn btn-primary">
					   <i class="fa fa-picture-o"></i> Choose
					 </a>
					</span>
					{{ Form::hidden('video', null, array( 'class' => 'form-control', 'id' => 'thumbnail-video') ) }}
					</div>
					<iframe id="holder-video" style="margin-top:15px;max-height:100px;" 
					<?php echo $post->video ? "src='".url($post->video)."'": '';?> 
					frameborder="0" class="video_preview" width="320" height="240"></iframe>

				  </div>
				  	
				  <div class="form-group">
					OR 
					<!-- video_url -->
					{{ Form::label('video_url', 'Video url') }}
					{{ Form::text('video_url', null, array( 'class' => 'form-control' ) ) }}

					<video width="320" height="240" style="margin-top:15px;max-height:100px;" controls>
					  <source src="<?php echo $post->video_url ? : '';?>" type="video/mp4">
					  <source src="<?php echo $post->video_url ? : '';?>" type="video/webm">
					  <source src="<?php echo $post->video_url ? : '';?>" type="video/ogg">
					  Your browser does not support the video tag.
					</video>

				  </div>
				  	
				  <div class="form-group">
					OR <!-- video_embed -->
					{{ Form::label('video_embed', 'Video Embed') }}
					{{ Form::textarea('video_embed', null, array( 'class' => 'form-control', 'rows' => '3') ) }}
					<?php echo $post->video_embed ? : '';?> 
				  </div>

					<div class="form-group">
					<!-- vdo_title -->
					{{ Form::label('video_title', 'Video title') }}
					{{ Form::text('video_title', null, array( 'class' => 'form-control' ) ) }}
					</div> 
				  
				</p>   
				</span>

			</div>

			<!-- /.box-body -->
			<div class="box-footer clearfix">
					{{ Form::submit('Save', array( 'class' => 'btn btn-primary' )) }}
					{{ Form::close() }}
					<a href=" {{ route( 'dashboard.post.index' ) }}" class="btn btn-default"/>Cancel</a>				
			</div>

		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">
	var post_edit_or_create = true;
</script>

@endsection
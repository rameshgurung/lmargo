@extends('backend.layout')

@section('title', 'Posts')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Posts</h3>
				<span class="pull-right">
					<a href="{{ route('dashboard.post.create') }}" title="Create">
						<button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
					</a>
				</span>

				<form action="" method="GET" role="form">
					<table class="table filter">            
						<tbody>
							<tr>
								<td class="col-lg-3">
									<div class="form-group">
										<label>Category</label>
										<select id="category_id" class="form-control" name="category_id">
											<option value="">Select</option>
											@foreach($categories as $category)
												<option value="{{ $category->id }}" 
													@if ( \Request::get('category_id') == $category->id ) 
														selected 
													@endif
													/>
													{{ $category->title }}
												</option>
											} ?>
											@endforeach
										</select>
									</div>
								</td>
								<td class="col-lg-3">
									<div class="form-group">
										<label>Author</label>
										<select id="author_id" class="form-control" name="author_id">
											<option value="">Select</option>
											@foreach($users as $user)
												<option value="{{ $user->id }}"
													@if ( \Request::get('author_id') == $user->id ) 
														selected 
													@endif
													/>
													{{ $user->name }}
												</option>
											} ?>
											@endforeach
										</select>
									</div>
								</td>
								<td class="col-lg-3">
									<div class="form-group">
										<label>Post Title</label>
										<div class="controls">
											<input type="text" name="title" id="title"  data-required="1" 
											class="form-control" placeholder="Post Title here"
											value="{{ \Request::get('title') }}">
										</div>
									</div>
								</td>
								<td class="col-lg-3">
									<div class="form-group pull-right">
										<br>
										<br>
										<div class="controls">
											<input type="submit" class="btn btn-info" name="filter" value="Filter">
											<a href=" {{ route( 'dashboard.post.index' ) }}" class="btn btn-default">Reset</a>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</form>

			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 10px">#</th>
							<th>Title</th>
							<th>Category</th>
							<th>Created At</th>
							<th>Actions</th>
						</tr>

						@if( count($posts) > 0 )
						@foreach($posts as $post)
						<tr>
							<td>{{ $post->id }}</td>
							<td>
								{{ $post->title }}
								<br>
								-- <i>by : {{ $post->author->name }}</i> -- 
							</td>
							<td>{{ $post->category->title }}</td>							
							<td>{{ $post->created_at }}</td>
							<td>													

								<a href=" {{ route( 'dashboard.post.edit' , $post->id ) }} " class="btn btn-default fa fa-pencil" data-toggle="tooltip" data-original-title="Edit"></a>

								<a href=" {{ route( 'dashboard.post.copy' , $post->id ) }} " class="btn btn-sm btn-icon add-tooltip btn-default fa fa-copy" data-toggle="tooltip" data-original-title="Copy"></a>

								{!! Form::open(array(
									'url' => URL::route( 'dashboard.post.destroy' , $post->id),
									'class' => 'form-delete' 
									)
								) 
								!!}
								{!! Form::hidden('_method', 'DELETE') !!}
								<button type="submit" class="btn btn-sm btn-danger fa fa-times delete" data-toggle="tooltip" data-original-title="Delete">
								</button>
								{!! Form::close() !!}

							</td>
						</tr>
						@endforeach
						@else
						<tr>
							<td colspan="5" class="text-center">No entries found</td>
						</tr>
						@endif

					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				
				<div class="col-sm-8">
					@if( count($posts) > 0 )
					Showing  {{ $posts->firstItem() }} to {{ $posts->lastItem() }} of {{ $posts->total() }} entries
					@endif
				</div>

				<div class="col-sm-4">					
					<ul class="pagination pagination-sm no-margin pull-right">
						{{ $posts->links() }}
					</ul>
				</div>

			</div>
		</div>
		<!-- /.box -->
	</div>
</div>

@endsection
@extends('backend.layout')

@section('title', 'Dashboard')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<!-- <div class="box-header with-border">
				<h3 class="box-title">Welcome to Dashboard</h3>
			</div>	 -->		

			@include('setting.index')

		</div>
		<!-- /.box -->
	</div>
</div>

@endsection



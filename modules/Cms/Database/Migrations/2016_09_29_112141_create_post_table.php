<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_post', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title',50);
            $table->string('slug',50)->unique();
            $table->text('content')->nullable();
            $table->string('excerpt',100)->nullable();
            $table->boolean('published');
            $table->string('type',10)->nullable();
            $table->string('format',10);
            $table->string('url',100)->nullable();
            
            $table->string('featured_image',100)->nullable();
            $table->string('featured_image_title',100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_post');
    }

}

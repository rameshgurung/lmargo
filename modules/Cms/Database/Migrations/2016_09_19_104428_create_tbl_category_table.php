<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCategoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_category', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',100);
            $table->string('title',100);
            $table->string('slug',50)->unique();
            $table->text('content')->nullable();
            $table->string('image',100)->nullable();
            $table->string('image_title',100)->nullable();
            $table->boolean('published');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_category');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_menu', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('title',50);
            $table->string('slug',50)->unique();
            $table->string('url',100)->nullable();            
            $table->string('page_type',10)->nullable();
            $table->string('menu_type',20)->default('UPPER');
            $table->string('breadcrumb',50)->nullable();
            $table->string('breadcrumb_desc',50)->nullable();
            $table->boolean('published')->default(1);
            $table->tinyInteger('level');
            $table->tinyInteger('order');
            $table->text('desc')->nullable();
            $table->boolean('sidebar');
            $table->text('custom_design')->nullable();

            $table->integer('category_id')->nullable()->unsigned();
            $table->foreign('category_id')->references('id')->on('tbl_category'); 

            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('users'); 

            $table->integer('post_id')->nullable()->unsigned();
            $table->foreign('post_id')->references('id')->on('tbl_post'); 

            $table->integer('parent_menu_id')->nullable()->unsigned();
            $table->foreign('parent_menu_id')->references('id')->on('tbl_menu'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_menu');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostImageLength extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
        Schema::table('tbl_post', function(Blueprint $table)
        {
            $table->string('images', 500)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}

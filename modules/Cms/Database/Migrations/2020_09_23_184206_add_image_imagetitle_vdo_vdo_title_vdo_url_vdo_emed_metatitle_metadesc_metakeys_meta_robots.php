<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageImagetitleVdoVdoTitleVdoUrlVdoEmedMetatitleMetadescMetakeysMetaRobots extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_post', function(Blueprint $table)
        {
			// $table->integer('order');
            $table->longText('images',50);
            $table->string('video',50);
            $table->string('video_title',50);
            $table->string('video_url',50);
            $table->string('video_embed',500);
            $table->string('meta_key',50);
            $table->string('meta_desc',50);
            $table->string('meta_robot',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_post', function(Blueprint $table)
        {
            // $table->dropColumn('order');
            // $table->dropColumn('image');
            // $table->dropColumn('images');
            // $table->dropColumn('image_title');
            // $table->dropColumn('video');
            // $table->dropColumn('video_title');
            // $table->dropColumn('video_url');
            // $table->dropColumn('video_embed');
            // $table->dropColumn('meta_key');
            // $table->dropColumn('meta_desc');
            // $table->dropColumn('meta_robot');
        });
    }

}

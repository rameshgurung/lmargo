<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePublishedDefault extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_category', function(Blueprint $table)
        {
            $table->boolean('published')->default('1')->change();
        });
        Schema::table('tbl_post', function(Blueprint $table)
        {
            $table->boolean('published')->default('1')->change();
            // $table->dropColumn('type');
            // $table->string('type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}

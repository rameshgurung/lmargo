<?php namespace Modules\Cms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Modules\Cms\Entities\Menu;
use Modules\Cms\Entities\Category;
use Modules\Cms\Entities\Post;

use Faker\Generator as Faker;
/** @var \Illuminate\Database\Eloquent\Factory $factory */

class MenuTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->command->info('Menu seed initailzed!');	

		// factory(Menu::class, 2)->create();

		// upper menu
		// level 0 - home, about us, blog, services, contact us
		$menus = array(
			/* 
				title, page_type,
					url, 
				breadcrumb, sidebar, level, order
			*/
			array('home', 'home', '', 0, 0, 1),
			array('about us', 'BLANK', 'about us', 1, 0, 2),
			array('blog', 'blog', 'blog', 0, 0, 3),
			array('services', 'CATEGORY', 'services', 1, 0, 4),
			array('contact us', 'CONTACT_US', 'contact us', 0, 0, 3),
			array('other pages', 'BLANK', 'other pages', 0, 0, 3),
		);
		
		foreach ($menus as $menu) {
			DB::table('tbl_menu')->insert([
				'title' => ucfirst($menu[0]),
				'slug' => str_slug($menu[0]),
				'page_type' => Menu::get_menu_page_types(strtoupper($menu[1])),
				'url' => '/'.str_slug($menu[0]),
				'breadcrumb' => ucfirst($menu[2]),
				'sidebar' => $menu[3],
				'level' => $menu[4],
				'order' => $menu[5],
				'author_id' => 8
			]);
			// 	factory(\Menu::class)->make([
			// 		'title' => ucfirst($menu[0]),
			// 		// 'page_type' => Menu::get_menu_page_types(strtoupper($menu[1])),
			// 		'url' => '/'.$menu[0],
			// 		'breadcrumb' => ucfirst($menu[2]),
			// 		'sidebar' => $menu[3],
			// 		'level' => $menu[4],
			// 		'order' => $menu[5],
			// 		]);
		}

		// level 1 - history, mission and vision
		$menus = array(
			array('history', 'CATEGORY', '', 0, 1, 1),
			array('mission', 'POST', 'mission', 1, 1, 2),
			array('vision', 'POST', 'vision', 0, 1, 3)
		);
		foreach ($menus as $menu) {
			DB::table('tbl_menu')->insert([
				'title' => ucfirst($menu[0]),
				'slug' => str_slug($menu[0]),
				'page_type' => Menu::get_menu_page_types(strtoupper($menu[1])),
				'url' => '/'.str_slug($menu[0]),
				'breadcrumb' => ucfirst($menu[2]),
				'sidebar' => $menu[3],
				'level' => $menu[4],
				'order' => $menu[5],
				'author_id' => 8
			]);

		}

		// other types - custom, not found, timeline		
		$parent_menu_id = DB::table('tbl_menu')->where('title', '=', 'Other pages')->first(['id'])->id;
		$menus = array(
			array('custom', 'custom', 'custom', 0, 1, 1),
			array('not found', 'notfound', 'notfound', 1, 1, 2),
			array('timeline', 'timeline', 'timeline', 0, 1, 3)
		);
		foreach ($menus as $menu) {
			DB::table('tbl_menu')->insert([
				'title' => ucfirst($menu[0]),
				'slug' => str_slug($menu[0]),
				'page_type' => Menu::get_menu_page_types(strtoupper($menu[1])),
				'url' => '/'.str_slug($menu[0]),
				'breadcrumb' => ucfirst($menu[2]),
				'sidebar' => $menu[3],
				'level' => $menu[4],
				'order' => $menu[5],
				'author_id' => 8,
				'parent_menu_id' => $parent_menu_id
			]);
		}

		// TODO: - DONE
		// category and post assigment, menu parent child
		// services, history, mission, vision

		$parent_menu_id = DB::table('tbl_menu')->where('title', '=', 'About us')->first(['id'])->id;
		$menus = array(
			array('services', 'category', DB::table('tbl_category')->where('title', '=', 'SERVICE')->first(['id'])->id ),
			array('history', 'category', DB::table('tbl_category')->where('title', '=', 'WORK')->first(['id'])->id ),
			array('mission', 'post', DB::table('tbl_post')->where('id', '=', 1)->first(['id'])->id ),
			array('vision', 'post', DB::table('tbl_category')->where('id', '=', 2)->first(['id'])->id ),
		);
		foreach ($menus as $menu) {
			if($menu[1] == "category"){
				$update = array('category_id' => $menu[2]);
			}else{
				$update = array('post_id' => $menu[2]);
			}
			$update['parent_menu_id'] = $parent_menu_id;
			DB::table('tbl_menu')->where('title', '=', ucfirst($menu[0]))->update($update);
		}

		// menu parent child
		// services, history, mission, vision

		$menus = array(
			array('services', 'category', DB::table('tbl_category')->where('title', '=', 'SERVICE')->first(['id'])->id ),
			array('history', 'category', DB::table('tbl_category')->where('title', '=', 'WORK')->first(['id'])->id ),
			array('mission', 'post', DB::table('tbl_post')->where('id', '=', 1)->first(['id'])->id ),
			array('vision', 'post', DB::table('tbl_category')->where('id', '=', 2)->first(['id'])->id ),
		);
		foreach ($menus as $menu) {
			if($menu[1] == "category"){
				$update = array('category_id' => $menu[2]);
			}else{
				$update = array('post_id' => $menu[2]);
			}
			DB::table('tbl_menu')->where('title', '=', ucfirst($menu[0]))->update($update);
		}


		// footer menu
		// level 0 - contact, privacy policy, sitemap
		$menus = array(
			array('contact us', 'CONTACT_US', '', 0, 0, 1),
			array('privacy policy', 'post', 'privacy policy', 1, 0, 2),
			array('sitemap', 'custom', 'sitemap', 0, 0, 3),
		);
		foreach ($menus as $menu) {
			DB::table('tbl_menu')->insert([
				'title' => ucfirst($menu[0]),
				'slug' => $menu[0] == "contact us" ? str_slug($menu[0])."-".rand(1,100) : str_slug($menu[0]) ,
				'page_type' => Menu::get_menu_page_types(strtoupper($menu[1])),
				'url' => '/'.str_slug($menu[0]),
				'breadcrumb' => ucfirst($menu[2]),
				'sidebar' => $menu[3],
				'level' => $menu[4],
				'order' => $menu[5],
				'author_id' => 8,
				'menu_type' => 'FOOTER'
			]);
		}

		$this->command->info('Menu seeded!');		
	}

}
<?php namespace Modules\Cms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CmsDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		// $this->call(CategoriesTableSeeder::class);
		// $this->call(SettingsTableSeeder::class);
		$this->call(MenuTableSeeder::class);
	}

}
<?php namespace Modules\Cms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

// use DB;
use Illuminate\Support\Facades\DB;
use Modules\Cms\Entities\Category;
use Modules\Cms\Entities\Post;

class CategoriesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		// DB::table('tbl_category')->insert([
		// 	'slug' => 'my_category',
		// 	'title' => 'My Category',
		// 	'content' => 
		// 	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		// 	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		// 	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		// 	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		// 	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		// 	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
		// 	]);


		factory(Category::class, 2)->create()->each(function($c) {	
			// Seed the relation with many posts
            $posts = factory(Post::class, 2)->make(['type' => NULL]);
            $c->posts()->saveMany($posts);
	    });

		// SERVICE category and posts
		$category = factory(Category::class)->create(['title' => 'SERVICE']);
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));

		// WORK category and posts
		$category = factory(Category::class)->create(['title' => 'WORK']);
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));

		// TESTIMONIAL category and posts
		$category = factory(Category::class)->create(['title' => 'TESTIMONIAL']);
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));

		// NEWS category and posts
		$category = factory(Category::class)->create(['title' => 'NEWS']);
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));
        $category->posts()->save(factory(Post::class)->make(['type' => NULL]));

		// blog category and blogs (posts)
		$category = factory(Category::class)->create(['title' => 'BLOG']);
        $category->posts()->save(factory(Post::class)->make(['type' => 'IMAGE']));
        $category->posts()->save(factory(Post::class)->make(['type' => 'GALLERY']));
        $category->posts()->save(factory(Post::class)->make(['type' => 'VIDEO']));

	}

}
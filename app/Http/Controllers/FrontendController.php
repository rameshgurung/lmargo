<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;

class FrontendController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function home()
{
    if(is_maintenance_mode_enabled()){
        $msg = maintenance_msg();
        return View::make('errors/custom_503', compact('msg'));        
    }
    return View::make('frontend.templates.home');       
}

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function getPage($url = null)
{
    if(is_maintenance_mode_enabled()){
        $msg = maintenance_msg();
        return View::make('errors/custom_503', compact('msg'));        
    }
    $pages = array(
        array(
            'title' => 'About Page',
            'url'   => 'pa',
            'content'   =>' About us page content',
            'template'  => 'page'
            ),
        array(
            'title' => 'Simple post',
            'url'   => 'po',
            'content'   =>' simple post content',
            'template'  => 'post'
            ),
        array(
            'title' => 'Contact Page',
            'url'   => 'c',
            'content'   =>' About us contact page',
            'template'  => 'page_contact'
            ),
        array(
            'title' => 'Archive Page',
            'url'   => 'a',
            'content'   =>' Archive page',
            'template'  => 'archive'
            ),
        );

    $page = array();

    foreach ($pages as $p) {
        if( $p['url'] == $url ){
            $page = $p; 
            break;
        }
    }

    if(count($page) > 0){
        $template = $page['template'];
        // return View::make("frontend.templates.$template", compact($page));
        if (View::exists("frontend.templates.$template")) {
            return View::make("frontend.templates.$template")->with('page', $page); 
        }else{
            return View::make('frontend.templates.404');                    
        }
    }
    else{
        return View::make('frontend.templates.404');        
    }

}

}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// load all ping pong modules route
foreach (Module::all() as $module) {
	$moduleRoute = $module->getPath(). '/Http/routes.php';
	require_once($moduleRoute);
}

// UniSharp Filemanager routes
Route::group(['middleware' => ['web','auth'], 'prefix' => 'dashboard', 'namespace' => 'Modules\Cms\Http\Controllers'], function(){
	
	Route::group(['prefix' => 'filemanager'], function () { 
	
		Route::get('/', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show')->name('dashboard.laravelfilemanager');

	}); 

});

// auth route
Route::auth();


// frontend routes
Route::get('/', [
	'as'      => 'home',
	'uses'    => 'FrontendController@home'
	]);

// Catch all page controller (place at the very bottom)
Route::get('{slug}', [
	'uses' => 'FrontendController@getPage' 
	], false)->where('slug', '([A-Za-z0-9\-\/]+)')->name('frontend.url');

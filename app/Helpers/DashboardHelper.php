<?php

use Modules\Cms\Entities\Post;
use Modules\Cms\Entities\Category;
use Modules\Cms\Entities\Setting;
use Modules\Cms\Entities\Menu;

if (!function_exists('get_blog_types')) {

    /**
     * Get blog types
     * @param  string $blog_type
     * @return string/array
     */
    function get_blog_types($blog_type='')
    {
        return Post::post_types($blog_type);
    }
}

if (!function_exists('get_blog_category')) {

    /**
     * [get_blog_category description]
     * @return object [eloquent model]
     */
    function get_blog_category()
    {
        $category_name = Category::blogCategoryTitle();
        return Category::where('name',$category_name)->firstOrFail(); 
    }
}

if (!function_exists('get_categories')) {

    /**
     * [get_blog_category description]
     * @return object [eloquent model]
     */
    function get_categories()
    {
        return Category::where('published',1)
        // ->whereNotIn('title', 'BLOG')
        ->get(['id','title']);
    }
}

if (!function_exists('get_settings')) {

    /**
     * [get_settings description]
     * @return objects [eloquent model]
     */
    function get_settings()
    {
        $settings = array();
        // $settings_db = Setting::where('autoload',1)->get();
        $settings_db = Setting::all();
        foreach ($settings_db as $setting) {
            $settings[$setting->slug] = $setting->value; 
        }
        return $settings; 
    }
}

if (!function_exists('maintenance_msg')) {

    /**
     * [maintenance_msg description]
     * @return objects [eloquent model]
     */
    function maintenance_msg()
    {
        return Setting::where('slug','maintenance_msg')->firstOrFail()->value;
    }
}

if (!function_exists('is_maintenance_mode_enabled')) {

    /**
     * [is_maintenance_mode_enabled description]
     * @return objects [eloquent model]
     */
    function is_maintenance_mode_enabled()
    {
        return Setting::where('slug','maintenance_mode_enabled')->firstOrFail()->value;
    }
}


if (!function_exists('get_posts')) {

    /**
     * [get_posts description]
     * @return objects [eloquent model]
     */
    function get_posts()
    {
        return Post::where('published',1)
        // ->whereNotIn('title', 'BLOG')
        ->get(['id','title']);
    }
}

if (!function_exists('get_menu_page_types')) {

    /**
     * Get blog types
     * @param  string $blog_type
     * @return string/array
     */
    function get_menu_page_types($menu_page_type='')
    {
        if($menu_page_type){
            return Menu::get_menu_page_types($menu_page_type);
        }else{
            $menu_page_types = Menu::get_menu_page_types($menu_page_type);
            unset( $menu_page_types[Menu::HOME] );
            unset( $menu_page_types[Menu::BLOG] );
            unset( $menu_page_types[Menu::CONTACT_US] );
            return $menu_page_types;
        }
    }
}

if (!function_exists('get_menu_types')) {

    /**
     * Get blog types
     * @param  string $blog_type
     * @return string/array
     */
    function get_menu_types($menu_type='')
    {
        return Menu::get_menu_types($menu_type);
    }
}

if (!function_exists('get_upper_parent_menus')) {

    /**
     * [get_upper_parent_menus description]
     * @return objects [eloquent model]
     */
    function get_upper_parent_menus()
    {
        // return array('m1', 'm2');
        return Menu::where('published',1)
        ->where('id','>', 1)
        ->where('menu_type', Menu::UPPER_MENU)
        ->where('level', 0)
        ->get(['id','title']);
    }
}

if (!function_exists('get_menu_page_types')) {

    /**
     * Get blog types
     * @param  string $page_type
     * @return string/array
     */
    function get_menu_page_types($page_type='')
    {
        return Menu::get_menu_page_types($page_type);
    }
}


if (!function_exists('image_exists')) {

    /**
     * [image_exists description]
     * @return default_image or image
     */
    function image_exists($image){
        if($image){
            if(file_exists(public_path($image))){
                return url($image);             
            }else{
                echo "d";
                return default_image();
            }
        }
    }
}

if (!function_exists('default_image')) {

    /**
     * [default_image description]
     * @return default default_image path
     */
    function default_image(){
        return url('images/no_photo_default.png');
    }
}


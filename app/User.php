<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Cms\Entities\Post;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // solve table name hardcode issue
    public static $table_name = 'users';


    public function posts(){
        return $this->hasMany(Post::class);
    }       
}

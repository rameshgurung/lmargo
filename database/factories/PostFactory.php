<?php

use Modules\Cms\Entities\Post;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->company,
		'slug' => $faker->slug,
		'content' => $faker->paragraph, 
		'content' => $faker->paragraph, 
		'excerpt' => $faker->paragraph,
		'published' => 1,
		'type' => 'post',
		'format' => 'default',
		'author_id' => 8,
    ];
});

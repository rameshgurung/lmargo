<?php

use Modules\Cms\Entities\Category;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
		'slug' => $faker->slug,
		'title' => $faker->company,
		'content' => $faker->paragraph, 
		'image' => $faker->image,
		'image_title' => $faker->text,
		'published' => 1
    ];
});

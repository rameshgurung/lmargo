
$(window).load(function () {
	$('a.delete').on("click", function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		bootbox.confirm("Are you sure?", function(result) {
			if(result){
				window.location = url;
			}
		});
	});
});


1. install ping pong
	composer dump-autoload

config/database.php vs .env
	That file (config/database.php) is the actual configuration file that will be used. That file just happens to pull some values from the env. This allows you to not have to change the config files themselves when you have a project on different servers. You can just have a different .env file on each host that specifies its unique values for those.
	Imagine you are working with other people. They will have different configuration for their database. Having to hardcode values into the config file that is committed to a repository would end up with some problems as they pull down changes their configs might get overwritten. The .env file is not committed and should not be as its unique to the 'host'.
	By having the config files pull values from env, each person (host) can have their own .env file with their own values without having to make changes to the actual config files.

Model::unguard() at seed			
	A mass-assignment vulnerability occurs when a user passes an unexpected HTTP parameter through a request, and that parameter changes a column in your database you did not expect. For example, a malicious user might send an is_admin parameter through an HTTP request, which is then mapped onto your model's create method, allowing the user to escalate themselves to an administrator.

laravel view no hint path defined for, ping pong
	return view('Modulename::index');
	to
	return view('modulename::index');

You can also try running the following commands in Terminal or Command:
	1. composer dump-auto or composer dump-auto -o 
	2. php artisan cache:clear 
	3. php artisan config:clear

form
	https://laravelcollective.com/docs/5.2/html#installation 

21-sep
	handle missing route
	code: 
		app/Exceptions/Handler
			public function render($request, Exception $e)
		    {
		        // handle missing route
		        if($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
		        {
		            // normal 404 view page feedback
		            return response()->view('errors.404');
		        }
		        return parent::render($request, $e);
		    }
		view/error/404.blade.php

15-Aug-2020
	- selected option at template file
		<option value="{{ $category->id }}" 
			@if ( \Request::get('category_id') == $category->id ) 
				selected 
			@endif
			/>
			{{ $category->title }}
		</option>	
	- relation 
		category has many posts
			public function posts(){
				return $this->hasMany(Post::class);
			} 		
		post belongsto category
			public function category()
			{
				return $this->belongsTo(Category::class);
			}   
	- seeding with relation
		C:\xampp5\htdocs\lmargo\modules\Cms\Database\Seeders\CategoriesTableSeeder.php
		factory(Category::class, 2)->create()->each(function($c) {
			
			// Seed the relation with many posts
		    $posts = factory(Post::class, 2)->make();
		    $c->posts()->saveMany($posts);

		});	 		

Sep-2020
	app/User.php
		namespace App;

		use Illuminate\Foundation\Auth\User as Authenticatable;
		use Modules\Cms\Entities\Post;

		class User extends Authenticatable
		{
		    protected $hidden = [
		        'password', 'remember_token',
		    ];

		    public function posts(){
		        return $this->hasMany(Post::class);
		    }       
		}

	post controller filter
		--
		        // 1
		        // $posts = Post::where('published', 1)
		        //         ->leftJoin(Post::$stable, 'tbl_post.category_id', '=', 'tbl_category.id')
		        //         ->leftJoin('users', 'tbl_post.author_id', '=', 'users.id')
		        //         ->where('tbl_post.category_id', \Request::get('category_id'))
		        //         ->where('tbl_post.author_id', \Request::get('author'))
		        //         ->select('tbl_post.*')
		        //         ->paginate(10);

		        // 2
		        // $query = Post::where('tbl_post.published',1);
		        // if (\Request::get('category_id')) {
		        //   $query = $query->leftJoin('tbl_category', 'tbl_post.category_id', '=', 'tbl_category.id')
		        //                 ->where('tbl_post.category_id', \Request::get('category_id'));
		        // }
		        // if (\Request::get('author_id')) {
		        //   $query = $query->leftJoin('users', 'tbl_post.author_id', '=', 'users.id')
		        //                 ->where('tbl_post.author_id', \Request::get('author_id'));
		        // }
		        // if (\Request::get('title')) {
		        //   $query = $query->where('tbl_post.title', 'like', \Request::get('title').'%');;
		        // }
		        // $query = $query->addSelect('tbl_post.*');
		        // $posts = $query->paginate(10);

			    // 3
		        $query = Post::where(Post::$table_name.'.published',1);
		        if (\Request::get('category_id')) {
		          $query = $query->leftJoin(Category::$table_name, Post::$table_name.'.category_id', '=', 'tbl_category.id')
		                        ->where(Post::$table_name.'.category_id', \Request::get('category_id'));
		        }
		        if (\Request::get('author_id')) {
		          $query = $query->leftJoin(Category::$table_name, Post::$table_name.'.author_id', '=', 'users.id')
		                        ->where(Post::$table_name.'.author_id', \Request::get('author_id'));
		        }
		        if (\Request::get('title')) {
		          $query = $query->where(Post::$table_name.'.title', 'like', \Request::get('title').'%');;
		        }
		        $query = $query->addSelect(Post::$table_name.'.*');
		        $posts = $query->paginate(10);
		--

	add new route for resrouce route
		Route::get('post/{post}/copy', 'PostController@copy')->name('dashboard.post.copy');
		Route::resource('post', 'PostController');


	copy post - replicate model
		$post = $post->replicate(); //copy attributes

			$post = Post::find($id);
			if(!$post){
			    Session::flash('error', 'Post not found');
			    return Redirect::route('dashboard.post.index');
			}
			$post = $post->replicate(); //copy attributes
			$post->save(); 

10/23
	seed
		https://scotch.io/tutorials/generate-dummy-laravel-data-with-model-factories#toc-factory-make

		manual insert - time consuming
		thanks to faker - not dry
		factory class
		factory function
			single - $user = factory(App\User::class)->create();
			multiple - $users  = factory(App\User::class, 3)->create();
		overriding values
			$user = factory(App\User::class)->create(['username' => 'pizzamuncher']);
		$factory->defineAs() ?
		relation
			factory(App\User::class, 50)->create()->each(function($u) {
			    $u->posts()->save(factory(App\Post::class)->make());
			});	
		while testing ?

	custom helper
		http://www.expertphp.in/article/how-to-create-custom-helper-functions-in-laravel-5
		
		built in
		custom
			location : app/Helpers/Helper.php
		loading
			Using Composer to Autoload Files
			Using service providers to load file
			Using a third party package

10/30
	seed
		hardcode data
			https://stackoverflow.com/questions/37369452/use-laravel-seed-and-sql-files-to-populate-database

		steps
			1. export table data as sql file
			2. save sql file into folder inside root of laravel app
			3. Create seed file and run that sql file

				class SettingsTableSeeder extends Seeder {

					public function run()
					{
						Model::unguard();
						
						// $this->call("OthersTableSeeder");
				        $path = 'doc/sql/tbl_setting.sql';
				        DB::unprepared(file_get_contents($path));
				        $this->command->info('Setting table seeded!');		
					}

				}			
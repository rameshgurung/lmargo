-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2020 at 06:51 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------
--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_setting` (`id`, `name`, `slug`, `value`, `autoload`) VALUES
(5, 'facebook_link', 'facebook_link', 'https://www.facebook.com/rotaryfishtail.org', 0),
(6, 'twitter_link', 'twitter_link', 'https://www.twitter.com/rotaryfishtail.org', 0),
(7, 'google_plus_link', 'google_plus_link', 'https://www.googleplus.com/rotaryfishtail.org', 0),
(8, 'Site Name', 'site_name', 'Margo', 0),
(9, 'Site Url', 'site_url', 'Rotary Club Of Pokhara Fishtail', 0),
(12, 'club_name', 'club_name', 'Margo', 1),
(15, 'slogan_head', 'slogan_head', 'Service above Self', 1),
(16, 'slogan_body', 'slogan_body', 'ROTARY INTERNATIONAL', 1),
(17, 'slogan_dist', 'slogan_dist', 'Dist. 32921', 1),
(18, 'other_site_1_name', 'other_site_1_name', 'Fishtail Fund', 1),
(19, 'other_site_1_url', 'other_site_1_url', 'http://www.fishtailfund.com/', 1),
(20, 'other_site_2_name', 'other_site_2_name', 'Annapurna Fund', 1),
(21, 'other_site_2_url', 'other_site_2_url', 'http://www.fishtailfund.np/e', 1),
(22, 'slider_show_total', 'slider_show_total', '1', 1),
(23, 'sidebar1_content', 'sidebar1_content', 'n', 1),
(24, 'sidebar2_content', 'sidebar2_content', '[We meet on every Friday]\r\nat Atithi Resort & Spa\r\nLakeside, Pokhara, Nepal.\r\nFeb-Oct. (6:00pm to 7:00pm) \r\nNov-Jan. (5:30pm to 6:30pm).', 1),
(25, 'footer_message', 'footer_message', 'MARGO - ALL RIGHTS RESERVED', 1),
(26, 'slider_category_id', 'slider_category_id', '73', 1),
(27, 'telephone_1', 'telephone_1', '+12 345 678 000', 1),
(28, 'telephone_2', 'telephone_2', '+12 345 678 001', 1),
(29, 'mobile_1', 'mobile_1', '+01 234 567 890', 1),
(30, 'mobile_2', 'mobile_2', '+977 98560-21479', 1),
(31, 'email_1', 'email_1', 'info@yourcompany.com', 1),
(32, 'email_2', 'email_2', 'contact@rotaryfishtail.org', 1),
(33, 'location', 'location', 'House-54/A, London, UK', 1),
(34, 'contact', 'contact', 'Save', 1),
(41, 'header_logo', 'header_logo', 'margo1.png', 1),
(42, 'sidebar1_picture', 'sidebar1_picture', 'rot-brazil.gif', 1),
(43, 'sidebar2_picture', 'sidebar2_picture', '232828-1406991727.jpg', 1),
(44, 'no_of_message_on_home_page', 'no_of_message_on_home_page', '3', 1),
(46, 'no_of_projects_on_home_page', 'no_of_projects_on_home_page', '3', 1),
(49, 'message_category_id', 'message_category_id', '39', 1),
(61, 'project_category_id', 'project_category_id', '40', 1),
(62, 'no_of_news_on_home_page', 'no_of_news_on_home_page', '5', 1),
(67, 'news_category_id', 'news_category_id', '6', 1),
(68, 'welcome_message', 'welcome_message', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident', 1),
(69, 'introduction', 'introduction', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident\n', 1),
(70, 'introduction_article_id', 'introduction_article_id', '74', 1),
(71, 'category_id', 'category_id', '41', 1),
(72, 'widget_slug', 'widget_slug', 'news', 1),
(73, 'name', 'name', '', 1),
(74, 'content', 'content', '', 1),
(75, '_wysihtml5_mode', '_wysihtml5_mode', '1', 1),
(76, 'news_uploaded_file', 'news_uploaded_file', '', 1),
(77, 'add', 'add', 'Submit', 1),
(78, 'purchase_texts', 'purchase_texts', 'Modern, Clean, Awesome, Cool, Great', 1),
(79, 'purchase_line1', 'purchase_line1', 'MARGO TEMPLATE IS READY FOR ', 1),
(80, 'purchase_line2', 'purchase_line2', 'BUSINESS, AGENCY OR CREATIVE PORTFOLIOS', 1),
(82, 'dribble_link', 'dribble_link', 'dribble_link', 1),
(83, 'linkedin_link', 'linkedin_link', 'linkedin_link', 1),
(84, 'flickr_link', 'flickr_link', 'flickr_link', 1),
(85, 'tumblr_link', 'tumblr_link', 'tumblr_link', 1),
(86, 'instagram_link', 'instagram_link', 'i', 1),
(87, 'vimeo_link', 'vimeo_link', 'v', 1),
(88, 'skype_link', 'skype_link', 's', 1),
(89, 'purchase_bg', 'purchase_bg', 'about-01.jpg', 1),
(90, 'footer_get_in_touch', 'footer_get_in_touch', 'Join our mailing list to stay up to date and get notices about our new releases!e', 1),
(91, 'footer_total_tweets', 'footer_total_tweets', '3', 1),
(92, 'footer_total_flickers', 'footer_total_flickers', '8', 1),
(93, 'services_category_id', 'services_category_id', '3', 1),
(94, 'purchase_artilce_id', 'purchase_artilce_id', '', 1),
(95, 'works_category_id', 'works_category_id', '4', 1),
(99, 'testimonials_category_id', 'testimonials_category_id', '5', 1),
(100, 'clients_category_id', 'clients_category_id', '78', 1),
(101, 'consumer_key', 'consumer_key', 'jLRaYJpTD4743Z5Fw8hP4GqPb', 1),
(102, 'consumer_secret', 'consumer_secret', 'D7UOxfBGz1yS6v6Ssny6MOHRIBhRC1ekLHIjFqmnxcTD74cX7U', 1),
(103, 'access_token', 'access_token', '1853963712-ga5WiEVqHdVgA6HRYRxfch3TAN2pSQh2pI8F9l9', 1),
(104, 'access_token_secret', 'access_token_secret', 'O5PR1mTnB1nHcZDXaZlHb10r6GsEmtAck8aC88ct6reC4', 1),
(105, 'twitter_screen_name', 'twitter_screen_name', 'CelosiaDesigns', 1),
(106, 'website', 'website', 'www.yourdomain.com', 1),
(107, 'address2', 'address2', '1234 Street Name, Bangladesh.', 1),
(108, 'contact_working_hr1', 'contact_working_hr1', '9am to 5pm', 1),
(109, 'contact_working_hr2', 'contact_working_hr2', '9am to 2pm', 1),
(110, 'contact_working_hr3', 'contact_working_hr3', 'Closed', 1),
(111, 'contact_information', 'contact_information', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 1),
(112, 'mailchimp_api_key', 'mailchimp_api_key', 'ceb5d99de9ecd2e17c2e7788efc3bb3f-us13', 1),
(113, 'mailchimp_api_list_id', 'mailchimp_api_list_id', '049dd702f0', 1),
(114, 'footer_copyright_message', 'footer_copyright_message', 'MARGO - ALL RIGHTS RESERVED', 1),
(115, 'twitter_total_tweets', 'twitter_total_tweets', '3', 1),
(116, 'twitter_consumer_key', 'twitter_consumer_key', 'jLRaYJpTD4743Z5Fw8hP4GqPb', 1),
(117, 'twitter_consumer_secret', 'twitter_consumer_secret', 'D7UOxfBGz1yS6v6Ssny6MOHRIBhRC1ekLHIjFqmnxcTD74cX7U', 1),
(118, 'twitter_access_token', 'twitter_access_token', '1853963712-ga5WiEVqHdVgA6HRYRxfch3TAN2pSQh2pI8F9l9', 1),
(119, 'twitter_access_token_secret', 'twitter_access_token_secret', 'O5PR1mTnB1nHcZDXaZlHb10r6GsEmtAck8aC88ct6reC4', 1),
(120, 'footer_custom_menu_item_title1', 'footer_custom_menu_item_title1', 'Sitemap', 1),
(121, 'footer_custom_menu_item_link1', 'footer_custom_menu_item_link1', '81', 1),
(122, 'footer_custom_menu_item_title2', 'footer_custom_menu_item_title2', 'Privacy Policy', 1),
(123, 'footer_custom_menu_item_link2', 'footer_custom_menu_item_link2', '56', 1),
(124, 'footer_custom_menu_item_title3', 'footer_custom_menu_item_title3', 'Contact', 1),
(125, 'footer_custom_menu_item_link3', 'footer_custom_menu_item_link3', '60', 1),
(126, 'footer_custom_menu_item_link_type2', 'footer_custom_menu_item_link_type2', 'article', 1),
(127, 'footer_custom_menu_item_link_type3', 'footer_custom_menu_item_link_type3', 'menu', 1),
(128, 'maintenance_title', 'maintenance_title', 'Maintenance Mode', 1),
(129, 'maintenance_msg', 'maintenance_msg', 'Sorry, Our Website is Temporarily Down for Maintenance. Please Check Back Again Soon .', 1),
(130, 'maintenance_mode_enabled', 'maintenance_mode_enabled', '0', 1),
(132, 'mailchimp_newsletter_category_id', 'mailchimp_newsletter_category_id', '82', 1),
(133, 'mailchimp_newsletter_enabled', 'mailchimp_newsletter_enabled', '0', 1),
(135, 'mailchimp_newsletter_footer_msg', 'mailchimp_newsletter_footer_msg', 'best of luck, margo', 1),
(136, 'blog_category_id', 'blog_category_id', '82', 1),
(137, 'sidebar_widgets', 'sidebar_widgets', 'popular_articles, categories', 1),
(138, 'mailchimp_api_campaign_id', 'mailchimp_api_campaign_id', '245', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

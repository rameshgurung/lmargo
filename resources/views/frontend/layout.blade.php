<!doctype html>
<html>
<head>
	@include('frontend.includes.head')
</head>

<body>
	
	<!-- Full Body Container -->
	<div id="container">

		<!-- Start Header Section -->
		<div class="hidden-header"></div>
		@include('frontend.includes.header')

		@yield('content')
		
		@include('frontend.includes.footer')
	</div>
	<!-- End Full Body Container -->
	<!-- Go To Top Link -->
	<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

	<div id="loader">
		<div class="spinner">
			<div class="dot1"></div>
			<div class="dot2"></div>
		</div>
	</div>
</div>


<script type="text/javascript" src="js/script.js"></script>

</body>

</html>


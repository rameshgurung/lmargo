@extends('frontend.layout')

@section('title', 'Post')

@section('content')


<div id="content">
	<div class="container">
		<div class="page-content">


			<div class="row">

				<div class="col-md-8">

					<!-- Classic Heading -->
					<h4 class="classic-title"><span>{{ $page['title'] }}</span></h4>

					<p>
						<!-- Start Contact Form -->
						<form role="form" class="contact-form" id="contact-form" method="post">
							<div class="form-group">
								<div class="controls">
									<input type="text" placeholder="Name" name="name">
								</div>
							</div>
							<div class="form-group">
								<div class="controls">
									<input type="email" class="email" placeholder="Email" name="email">
								</div>
							</div>
							<div class="form-group">
								<div class="controls">
									<input type="text" class="requiredField" placeholder="Subject" name="subject">
								</div>
							</div>

							<div class="form-group">

								<div class="controls">
									<textarea rows="7" placeholder="Message" name="message"></textarea>
								</div>
							</div>
							<button type="submit" id="submit" class="btn-system btn-large">Send</button>
							<div id="success" style="color:#34495e;"></div>
						</form>
						<!-- End Contact Form -->					
					</p>

				</div>

			</div>

		</div>
	</div>
</div>

@endsection

@extends('frontend.layout')

@section('title', 'Post')

@section('content')


<div id="content">
	<div class="container">
		<div class="page-content">


			<div class="row">

				<div class="col-md-7">

					<!-- Classic Heading -->
					@if(isset($page))
					<h4 class="classic-title"><span>{{ $page['title'] }}</span></h4>

					<p>
						{{ $page['content'] }}						
					</p>
					@endif

				</div>

			</div>

		</div>
	</div>
</div>

@endsection

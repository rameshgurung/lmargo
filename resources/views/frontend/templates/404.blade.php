@extends('frontend.layout')

@section('title', '404 Page not found')

@section('content')


<div id="content">
	<div class="container">
		<div class="page-content">


			<div class="row">

				<div class="col-md-7">

					<!-- Classic Heading -->
					<h4 class="classic-title"><span>404 page not found</span></h4>

				</div>

			</div>
			
		</div>
	</div>
</div>

@endsection

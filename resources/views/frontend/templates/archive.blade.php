@extends('frontend.layout')

@section('title', 'Archive')

@section('content')


<div id="content">
	<div class="container">
		<div class="page-content">


			<div class="row">

				<div class="col-md-7">

					<!-- Classic Heading -->
					<h4 class="classic-title"><span>{{ $page['title'] }}</span></h4>

					<p>
						{{ $page['content'] }}						
					</p>

				</div>

			</div>

		</div>
	</div>
</div>

@endsection

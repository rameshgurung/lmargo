@extends('auth.layout')

@section('title','Register')

@section('content')
<div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
            <input type="text" class="form-control" placeholder="Full name" name="name" value="{{ old('name') }}" >
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
        <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}" >
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @if ($errors->has('username'))
            <span class="help-block">
            <strong>{{ $errors->first('username') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" >
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
            @endif
        </div>
        <div class="row">        
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    {{-- <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-social btn-facebook btn-flat">
            <i class="fa fa-facebook"></i> 
            Sign up using
            Facebook
        </a>
        <a href="#" class="btn btn-block btn-social btn-google btn-flat">
            <i class="fa fa-google-plus"></i> 
            Sign up using
            Google+
        </a>
    </div> --}}

    <a href="{{ url('/login') }}" class="text-center">I already have a membership</a>
</div>


@endsection

<!doctype html>
<html>
<head>
	@include('backend.includes.head')
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		@include('backend.includes.header')
		<!-- jQuery 2.2.0 -->
		<script src="{{ asset('backend/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
		@include('backend.includes.sidebar')

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			@include('backend.includes.breadcrumbs')

			<!-- Main content -->
			<section class="content">
				@include('backend.includes.flash')
				@yield('content')				
			</section>
		</div>

		@include('backend.includes.footer')
	</div>
	
	<!-- ./wrapper -->

	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="{{ asset('backend/bootstrap/js/bootstrap.min.js') }}"></script>

	<!-- daterangepicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="{{ asset('backend/plugins/daterangepicker/daterangepicker.js') }}"></script>
	<!-- datepicker -->
	<script src="{{ asset('backend/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{ asset('backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
	<!-- Slimscroll -->
	<script src="{{ asset('backend/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
	<!-- FastClick -->
	<script src="{{ asset('backend/plugins/fastclick/fastclick.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('backend/dist/js/app.min.js') }}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	{{-- <script src="{{ asset('backend/dist/js/pages/dashboard.js') }}"></script> --}}
	<!-- AdminLTE for demo purposes -->
	{{-- <script src="{{ asset('backend/dist/js/demo.js') }}"></script> --}}

	<script src="{{ asset('backend/dist/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('backend/dist/js/custom.js') }}"></script>

	<!-- tinymce -->
	<script src="{{ asset('backend/plugins/lmargo/tinymce.min.js') }}"></script>
	<script>
		$(function(){
			$('button.delete').on("click", function(e) {
				e.preventDefault();
				var form = $(this).parent('form');
				bootbox.confirm("Are you sure ?", function(result) {
					if(result){
						form.submit();
					}
				});
			});


			// tinmymce
		    try{

		      tinyMCE.DOM.addClass('table', 'bod');


		      $('[data-toggle="tooltip"]').tooltip(); 

		      $('<br class="clear">').insertAfter('ul.pagination');


		      $('.textarea').wysihtml5();

		      var validate='0';
		      // console.log(validate);
		      if(validate==1){
		        $("#validate_form").validate();
		      }

		      $('a.delete').on("click", function(e) {
		        e.preventDefault();
		        var url = $(this).attr('href');
		        bootbox.confirm("Are you sure?", function(result) {
		          if(result){
		            window.location=url;
		          }
		        });
		      });

		      /*
		      tinymce.init({
		        selector: "#mytextarea",
		        fontsize_formats: "8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 30pt 36pt 40pt",
		        theme: "modern",
		        height: 400,
		        menubar: true,
		        browser_spellcheck : true,
		        codemirror: {
		          path: '../../../codemirror',
		          indentOnInit: true,
		          extraKeys: {
		            'Ctrl-Space': 'autocomplete'
		          },
		          config: {
		            lineNumbers: true,
		          }
		        },
		        body_id: "my_id",
		        image_advtab: true,
		        content_css: "http://localhost/cms/templates/front/css/style.css, http://localhost/cms/templates/front/css/layout.css",
		        plugins: [
		        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars fullscreen insertdatetime media nonbreaking",
		        "save table contextmenu directionality emoticons template paste textcolor"
		        ],
		        toolbar: "fullscreen | undo redo | fullpage | styleselect | removeformat | fontselect fontsizeselect | forecolor backcolor emoticons | bold italic underline | strikethrough superscript subscript | fontsize | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code preview print | link image | template", 
		        templates : [
		        {
		          title: "Club Member's Information",
		          url: "http://localhost/cms/article/club_member/add",
		          description: "Club Member's Information description"
		        }],
		        external_filemanager_path:"http://localhost/cms/plugin/filemanager/",
		        filemanager_title:"Filemanager" ,
		        external_plugins: { "filemanager" : "http://localhost/cms/templates/assets/editor/filemanager/plugin.min.js" },
		        style_formats: [
		        {title: 'Heading 1', block: 'h1'},
		        {title: 'Heading 2', block: 'h2'},
		        {title: 'Heading 3', block: 'h3'},
		        {title: 'Heading 4', block: 'h4'},
		        {title: 'Heading 5', block: 'h5'},
		        {title: 'Heading 6', block: 'h6'},
		        {title: 'Blockquote', block: 'blockquote', styles: {color: '#333'}},
		        {title: 'Pre Formatted', block: 'pre'},
		        {title: 'code', block: 'pre', classes: 'code'},
		        ],
		        relative_urls: false,
		        remove_script_host : true
		      });
		      */

		      // tinymce.init({
		      //   selector: "#mytextarea",theme: "modern", height: 200,
		      //   plugins: [
		      //   "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		      //   "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		      //   "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
		      //   ],
		      //   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		      //   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		      //   image_advtab: true ,
		      //   relative_urls : false,
		      //   remove_script_host : false,
		      //   convert_urls : true,

		      //   // to save <video> tag in db
		      //   extended_valid_elements: 'video',


		      //   external_filemanager_path:"{{ asset('backend/plugins/lmargo/filemanager/') }}" + '/',
		      //   filemanager_title:"Responsive Filemanager" ,
		      //   external_plugins: { "filemanager" : "{{ asset('backend/plugins/lmargo/filemanager/plugin.min.js') }}"}
		      // });

				var editor_config = {
				    path_absolute : "{{ route( 'dashboard' ) }}",
				    selector: "#mytextarea",
				    plugins: [
				      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
				      "searchreplace wordcount visualblocks visualchars code fullscreen",
				      "insertdatetime media nonbreaking save table contextmenu directionality",
				      "emoticons template paste textcolor colorpicker textpattern"
				    ],
					// plugins: [
					//   "advlist autolink link image lists charmap print preview hr anchor pagebreak",
					//   "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
					//   "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
					//   ],
					// image_advtab: true ,
					// convert_urls : true,
				    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
			        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
			        toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
				    relative_urls: false,

					// // to save <video> tag in db
					// extended_valid_elements: 'video',

				    file_browser_callback : function(field_name, url, type, win) {
				      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
				      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

				      var cmsURL = editor_config.path_absolute + '/filemanager?field_name=' + field_name;
				      if (type == 'image') {
				        cmsURL = cmsURL + "&type=Images";
				      } else {
				        cmsURL = cmsURL + "&type=Files";
				      }

				      tinyMCE.activeEditor.windowManager.open({
				        file : cmsURL,
				        title : 'Filemanager',
				        width : x * 0.8,
				        height : y * 0.8,
				        resizable : "yes",
				        close_previous : "no"
				      });
				    }
				  };

				  tinymce.init(editor_config);

			    }
		    catch(e){
		      console.log(e.message)
		    }

		});
	</script>

	<script>
		if (typeof (blog_edit_or_create) != "undefined") {

			var blog_type = $('select[name="type"]').val();
			var gallery_images = typeof(blog_gallery_images)!="undefined"?blog_gallery_images:[];

			$(function () {

				toggle_blog_panel(blog_type);

				$('select[name="type"]').change(function () {
					blog_type = $(this).val();
					toggle_blog_panel(blog_type)
				});

				function toggle_blog_panel(blog_type) {
					switch (blog_type) {
					case 'IMAGE':
						{
							$('.blog_type_gallery').hide();
							$('.blog_type_video').hide();
							$('.blog_type_image').show();
							// filemanager_modal('Select Image', 'http://localhost/cms/filemanager/image');
							$('#lfm-image').filemanager('image', {
								prefix: "{{ route( 'dashboard.laravelfilemanager' ) }}"
							});
							break;
						}
					case 'GALLERY':
						{
							blog_type = 'GALLERY';
							$('.blog_type_image').hide();
							$('.blog_type_video').hide();
							$('.blog_type_gallery').show();
							$('#lfm-image-gallery').filemanager('image', {
								prefix: "{{ route( 'dashboard.laravelfilemanager' ) }}"
							});
							break;
						}
					case 'VIDEO':
						{
							$('.blog_type_gallery').hide();
							$('.blog_type_image').hide();
							$('.blog_type_video').show();
							$('#lfm-video').filemanager('video', {
								prefix: "{{ route( 'dashboard.laravelfilemanager' ) }}"
							});
							break;
						}
					}
				};
			});

			$("body").on("click", "a.remove", function (e) {
				e.preventDefault();
				$(this).parent().remove();
				// todo - remove from js array (gallery_images)
			});

			var app_public_url = '<?php echo url('/');?>';

			function set_gallery_images(file_path) {
				if(blog_type == "GALLERY"){
					gallery_images.push(file_path);
					generate_gallery_images(file_path);
				}
			}
			function generate_gallery_images(file_path){
				$('.blog_type_gallery #holder-image-gallery').remove();
				$('.blog_type_gallery .gallery_image_block').remove();
				var html = '';
				for (var i = 0; i < gallery_images.length; i++) {
					html = html + '<div class="col-md-2 gallery_image_block"><input name="images[]" type="hidden" value="'+ gallery_images[i] + '"><img src="' + app_public_url+gallery_images[i] + '" class="gallery_image" width="65" height="65"><a href="#" class="btn btn-xs btn-danger remove">Remove</a></div>';
				}
				$('.blog_type_gallery .input-group').after(html);					
			}
			
			if (gallery_images.length > 0) {
				$.each(gallery_images, function(i, gallery_image) {
					generate_gallery_images(gallery_image);
				})
			}
		}		
	</script>


	<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
	<script type="text/javascript">
		if (typeof (post_edit_or_create) != "undefined") {
			$('#lfm-image').filemanager('image', {prefix: "{{ route( 'dashboard.laravelfilemanager' ) }}"});
			$('#lfm-video').filemanager('video', {prefix: "{{ route( 'dashboard.laravelfilemanager' ) }}"});
		}
	</script>

	<style type="text/css">
		.gallery_image_block{
			margin-top: 4%;
		}		
		.gallery_image_block .btn{
			margin-left: 5%;
		}		
		.gallery_image {
		    display: block;
		    padding: 4px;
		    margin-bottom: 20px;
		    line-height: 1.42857143;
		    background-color: #fff;
		    border: 1px solid #ddd;
		    border-radius: 4px;
		    -webkit-transition: border .2s ease-in-out;
		    -o-transition: border .2s ease-in-out;
		    transition: border .2s ease-in-out;
		}
	</style>
</body>
</html>

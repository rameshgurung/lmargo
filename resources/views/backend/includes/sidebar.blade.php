<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">CONTENT</li>
      
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-navicon"></i> <span>Menu</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="{{ route('dashboard.menu.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
          <li><a href="{{ route('dashboard.menu.create') }}"><i class="fa fa-circle-o"></i> Add </a></li>
          <li><a href="{{ route('dashboard.menu.order') }}"><i class="fa fa-circle-o"></i> Order </a></li>
          <li class="">
            <a href="#"><i class="fa fa-circle-o"></i> Footer Menu<i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu" style="display: none;">
              <li><a href="{{ route('dashboard.menu.footer.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
              <li><a href="{{ route('dashboard.menu.footer.create') }}"><i class="fa fa-circle-o"></i> Add</a></li>
            </ul>
          </li>          
        </ul>
      </li> 

      <li class="active treeview">
        <a href="#">
          <i class="fa fa-list-ul"></i> <span>Category</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="{{ route('dashboard.category.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
          <li><a href="{{ route('dashboard.menu.create') }}"><i class="fa fa-circle-o"></i> Add </a></li>
        </ul>
      </li> 

      <li class="active treeview">
        <a href="#">
          <i class="fa fa-newspaper-o"></i> <span>Post</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="{{ route('dashboard.post.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
          <li><a href="{{ route('dashboard.post.create') }}"><i class="fa fa-circle-o"></i> Add </a></li>
        </ul>
      </li> 


      <li class="active treeview">
        <a href="#">
          <i class="fa fa-newspaper-o"></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="{{ route('dashboard.blog.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
          <li><a href="{{ route('dashboard.blog.create') }}"><i class="fa fa-circle-o"></i> Add </a></li>
        </ul>
      </li> 


      <li class="header">Misc
        <li><a href="{{ route('unisharp.lfm.show') }}"><i class="fa fa-file-image-o"></i> <span>File Manager</span></a></li>
        <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
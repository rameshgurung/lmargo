<header class="main-header">
	<!-- Logo -->
	<a href="{{url('dashboard')}}" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>S</b>C</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Simple </b>CMS</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs">
							
							@if(Auth::user())
							{{ Auth::user()->username }}
							@endif

						</span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">

							<p>
								@if(Auth::user())
								{{ Auth::user()->name }}
								<br>
								{{ Auth::user()->created_at }}
								@endif
							</p>
						</li>						
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>				
			</ul>
		</div>
	</nav>
</header>
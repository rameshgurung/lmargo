@if(Session::has('error') || count($errors) > 0)
<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">×</span>
		<span class="sr-only">Close</span>
	</button>

	<strong>Whoops! Something went wrong!</strong>


	@if(Session::has('error')) 
	<p>{{ Session::get('error') }}</p>
	@endif

	@if (count($errors) > 0)
	@foreach ($errors->all() as $error)
	<p>{{ $error }}</p>
	@endforeach
	@endif
	
</div>
@endif

@if(Session::has('success'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">×</span>
		<span class="sr-only">Close</span>
	</button>
	<p>{{ Session::get('success') }}</p>
</div>
@endif